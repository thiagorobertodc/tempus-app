import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { Camera } from '@ionic-native/camera/ngx';
import {Crop} from '@ionic-native/crop/ngx';
import { CameraService } from './camera.service';


describe('CameraService', () => {
  let service: CameraService;
  let cameraSpy, cropSpy;
  beforeEach(() => {
    cameraSpy = jasmine.createSpyObj('Camera', ['getPicture']);
    cropSpy = jasmine.createSpyObj('Crop', ['crop']);
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      providers: [
        {provide: Camera, useValue: cameraSpy},
        {provide: Crop, useValue: cropSpy},
      ]
    });
    service = TestBed.inject(CameraService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
