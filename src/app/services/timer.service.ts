import { Injectable } from '@angular/core';
import { ParseService } from './parse.service';

@Injectable({
  providedIn: 'root'
})
export class TimerService {
  private beginTime: any;
  private endTime: any;
  constructor(private parse: ParseService) {
  }

  public startTimer() {
    this.beginTime = new Date();
  }

  public async stopTimer(task: any) {
    this.endTime = new Date();
    await this.calculateAndSave(task);
  }

  public async calculateAndSave(taskNumber: any) {
    let user = this.parse.currentUser;
    let taskString = taskNumber.toString();
    let finalTime = (+this.endTime.valueOf() - (this.beginTime != null ? +this.beginTime.valueOf() : this.endTime.valueOf())) / 1000;
    let tasks = user.get('tasks') ?? {};
    tasks = {...tasks, [taskString]: finalTime + ' segundos'};
    await this.parse.updateUser({tasks: tasks});
  }
}
