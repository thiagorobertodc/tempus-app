import {Injectable} from '@angular/core';
import {UtilsService} from './utils.service';
import {FormGroup} from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class ValidationService {

  constructor(public utils: UtilsService) {
  }

  public formValidation(form: FormGroup) {
    for (const att in form.controls) {
      if ((form.get(att).value === '' || form.get(att).value == null) && (att !== 'complement' && att !== 'photo')) {
        this.utils.showToaster('Preencha todos os campos obrigatórios', 4000);
        return false;
      }
    }
    return true;
  }
}
