import {Injectable} from '@angular/core';
import {AlertController, LoadingController, NavController, Platform, ToastController} from '@ionic/angular';
import {Router} from '@angular/router';
import {FormGroup} from '@angular/forms';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UtilsService {

  public mainToast;
  public loading;

  constructor(public alertCtrl: AlertController,
              public toastCtrl: ToastController,
              public loadingCtrl: LoadingController,
              public router: Router,
              public navCtrl: NavController,
              public platform: Platform) {
  }

  async showAlert(header: string, message: string) {
    let choice;
    const alert = await this.alertCtrl.create({
      header,
      message,
      cssClass: 'alert-danger',
      mode: 'md',
      buttons: [
        {
          text: 'Não',
          role: 'cancel',
          handler: () => {
            alert.dismiss(false);
            return false;
          }
        }, {
          text: 'Sim',
          role: 'selected',
          cssClass: 'confirm-delete',
          handler: () => {
            alert.dismiss(true);
            return true;
          }
        }
      ]
    });
    await alert.present();
    await alert.onDidDismiss().then((data) => {
      choice = data;
    });

    return choice;
  }

  async showOKAlert(header: string, message: string) {
    let choice;
    const alert = await this.alertCtrl.create({
      header,
      message,
      cssClass: 'alert-ok-button',
      mode: 'md',
      buttons: [
        {
          text: 'OK',
          role: 'cancel',
          handler: () => {
            alert.dismiss(false);
            return false;
          }
        }
      ]
    });
    await alert.present();
    await alert.onDidDismiss().then((data) => {
      choice = data;
    });

    return choice;
  }

  public showToaster(message: string, duration?: number) {
    this.mainToast = this.toastCtrl.create({
      message,
      duration: duration ? duration : null
    }).then((toast) => {
      toast.present();
    });
    // return this.mainToast;
  }

  public showComingSoonToaster() {
    this.mainToast = this.toastCtrl.create({
      message: 'Disponível em breve',
      duration: 2000
    }).then((toast) => {
      toast.present();
    });
    // return this.mainToast;
  }
  // public openUrl(url: string) {
  //   window.open(environment.URLs.domain + url, '_system');
  // }


  async showLoading(message = null, time = null) {

    if (await this.loadingCtrl.getTop() === undefined) {
      this.loading = await this.loadingCtrl.create({
        message: message || null,
        spinner: null,
        duration: time || 30000,
        cssClass: 'custom-load',
        backdropDismiss: false
      });
      await this.loading.present();
    }
  }

  dismissLoading() {

    if (this.loading) {
      this.loading.dismiss();
    }
  }

  public parseError(e) {

    this.dismissLoading();
    // se for erro de autenticação
    if (e.code === 209) {
      this.router.navigateByUrl('/login');
    }

    let message = 'Erro';
    switch (e.code) {
      case 0:
        message = e.message;
        break;
      case 101:
        message = 'Não encontrado';
        break;
      case 137:
        message = 'Objeto não encontrado no banco de dados';
        break;
      case 137:
        message = 'Esse cadastro já se encontra no banco de dados';
        break;
      case 141:
        message = e.message; // erro enviado pelo cloud functions
        break;
      case 202:
        message = 'Já existe um usuário com este e-mail';
        break;
      default:
        // console.log(JSON.stringify(e));
        message = '' + e.message + '';
        break;
    }

    if (message.length) {
      this.showToaster(message, 4000);
    }

    // se tiver um loading na página
    if (this.loading) {
      try {
        this.loading.dismiss();
      } catch (e) {
        console.log(e);
      }
    }
  }

  public minMaxLatLng(lat, lng, radiusInKm) {

    const kmInLongitudeDegree = 111.320 * Math.cos((lat / 180.0) * Math.PI);
    const deltaLat = radiusInKm / 111.1;
    const deltaLong = radiusInKm / kmInLongitudeDegree;

    return {
      minLat: lat - deltaLat,
      maxLat: lat + deltaLat,
      minLong: lng - deltaLong,
      maxLong: lng + deltaLong
    };
  }

  public convertToDataURLviaCanvas(url, outputFormat) {
    return new Promise((resolve, reject) => {
      const img = new Image();
      img.crossOrigin = 'Anonymous';
      img.onload = () => {
        let canvas = document.createElement('CANVAS') as HTMLCanvasElement,
            ctx = canvas.getContext('2d'),
            dataURL;
        canvas.height = img.height;
        canvas.width = img.width;
        ctx.drawImage(img, 0, 0);
        dataURL = canvas.toDataURL(outputFormat);
        resolve(dataURL);
        canvas = null;
      };
      img.src = url;
    });
  }

  public onKeyUp(event: any, form: FormGroup, field: string) {

    let newValue = event.target.value;

    let regExp = new RegExp('^[A-Za-z? ]+$');

    if (!regExp.test(newValue)) {
      form.controls[field].setValue(newValue.slice(0, -1));
    }
  }

  public onKeyUpHybrid(event: any, form: FormGroup, field: string) {

    let newValue = event.target.value;

    let regExp = new RegExp('^[A-Za-z-0-9? ]+$');

    if (!regExp.test(newValue)) {
      form.controls[field].setValue(newValue.slice(0, -1));
    }
  }

  public getCardFlag(cardnumber: any) {
    var cardnumber = cardnumber.replace(/[^0-9]+/g, '');

    // Visa / Master / Amex / Elo / Aura / JCB / Diners / Discover / Hipercard / Hiper

    var cards = {
      Visa: /^4[0-9]{12}(?:[0-9]{3})/,
      Master: /^5[1-5][0-9]{14}/,
      Diners: /^3(?:0[0-5]|[68][0-9])[0-9]{11}/,
      Amex: /^3[47][0-9]{13}/,
      Discover: /^6(?:011|5[0-9]{2})[0-9]{12}/,
      Hipercard: /^(606282\d{10}(\d{3})?)|(3841\d{15})/,
      Elo: /^((((636368)|(438935)|(504175)|(451416)|(636297))\d{0,10})|((5067)|(4576)|(4011))\d{0,12})/,
      JCB: /^(?:2131|1800|35\d{3})\d{11}/,
      Aura: /^(5078\d{2})(\d{2})(\d{11})$/
    };

    for (var flag in cards) {
      if (cards[flag].test(cardnumber)) {
        return flag;
      }
    }
    return false;
  }

  public giveUp() {
    this.showAlert('Desistir da compra', 'Tem certeza que deseja desistir da compra? Se optar por comprar mais tarde,' +
        ' os ingressos podem estar esgotados.')
        .then(async res => {
          if (res.role !== 'cancel') {
            await this.showLoading();
            await this.navCtrl.navigateRoot('/tabs/tab2', {replaceUrl: true});
            this.dismissLoading();
          }
        });
  }

  public parserAddress(mapObject) {
    let response = {
      number: this.findInMap(mapObject, 'street_number'),
      street: this.findInMap(mapObject, 'route') || this.findInMap(mapObject, 'street_address'),
      neighborhood: this.findInMap(mapObject, 'sublocality_level_1') || this.findInMap(mapObject, 'neighborhood'),
      city: this.findInMap(mapObject, 'administrative_area_level_2'),
      state: this.findInMap(mapObject, 'administrative_area_level_1'),
      stateShort: this.findInMapShort(mapObject, 'administrative_area_level_1'),
      country: this.findInMap(mapObject, 'country'),
      postalcode: this.findInMap(mapObject, 'postal_code')
    };
    // console.log(response);
    return response;
  }

  findInMap(mapObject, type) {
    for (const component of mapObject.address_components) {
      for (const typeC of component.types) {
        if (typeC === type) {
          return component.long_name;
        }
      }
    }
    return false;
  }

  findInMapShort(mapObject, type) {
    for (const component of mapObject.address_components) {
      for (const typeC of component.types) {
        if (typeC === type) {
          return component.short_name;
        }
      }
    }
    return false;
  }

  getFee(value, config) {
    if (config.fee_type === 'fix') {
      return config.fee_fix;
    } else {
      return (value * config.fee_pct / 100);
    }
  }


}
