import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { ParseService } from './parse.service';

describe('ParseService', () => {
  let service: ParseService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule]
    });
    service = TestBed.inject(ParseService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
