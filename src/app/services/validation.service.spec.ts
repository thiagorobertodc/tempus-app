import { TestBed } from '@angular/core/testing';

import { ValidationService } from './validation.service';

import {RouterTestingModule} from "@angular/router/testing";
describe('ValidationService', () => {
  let service: ValidationService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule]
    });
    service = TestBed.inject(ValidationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
