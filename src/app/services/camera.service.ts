import {Injectable} from '@angular/core';
import {Camera, CameraOptions} from '@ionic-native/camera/ngx';
import {Platform} from '@ionic/angular';
import {Crop} from '@ionic-native/crop/ngx';

@Injectable({
  providedIn: 'root'
})
export class CameraService {

  constructor(public camera: Camera, public platform: Platform, public crop: Crop) {

  }

  public getMediaDefault(camera = true, height = null, width = null): Promise<any> {
    const options: CameraOptions = {
      quality: 80,
      correctOrientation: true,
      allowEdit: false,
      sourceType: camera ? this.camera.PictureSourceType.CAMERA : this.camera.PictureSourceType.PHOTOLIBRARY,
      mediaType: this.camera.MediaType.PICTURE,
      encodingType: this.camera.EncodingType.JPEG,
      destinationType: this.camera.DestinationType.FILE_URI,
      targetHeight: height || 600,
      targetWidth: width || 600
      // destinationType: this.camera.DestinationType.DATA_URL
    };
    return new Promise((resolve, reject) => {
      this.camera.getPicture(options).then((fileUri) => {

        if (this.platform.is('android')) {
          fileUri = 'file://' + fileUri;
        }

        this.crop.crop(fileUri, {quality: 100}).then((path) => {
          resolve({error: false, data: path});
          console.log(path);
        }).catch((error) => {
          // console.log('eerr!: ' + error);
          reject({error: true, data: error});
        });
      }).catch((error) => {
            // console.log('eerr2!: ' + error);
            reject({error: true, data: error});
          }
      );
    });
  }
}

