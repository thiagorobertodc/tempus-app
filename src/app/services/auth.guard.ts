import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot} from '@angular/router';
import {NavController} from '@ionic/angular';
import {ParseService} from "./parse.service";

@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate {
    constructor(public navCtrl: NavController,
                public parse: ParseService) {
    }

    async canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const user = await this.parse.getUser();
        // logged in so return true
        if (user) {
            return true;
        }

        await this.navCtrl.navigateRoot('/login', {replaceUrl: true});
        return false;

    }
}
