import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { TestBed, async } from '@angular/core/testing';

import { ModalController, Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import {Base64} from "@ionic-native/base64/ngx";
import { AppComponent } from './app.component';
import {RouterTestingModule} from "@angular/router/testing";
import {File} from '@ionic-native/file/ngx';
import {CameraService} from "./services/camera.service";
import { ParseService } from './services/parse.service';
import { TimerService } from './services/timer.service';

class MockPlatform {
  ready: jasmine.Spy<any>;
  backButton: any;
}

class MockBackButton {
  subscribeWithPriority: jasmine.Spy<any>;
}

describe('AppComponent', () => {

  let statusBarSpy, splashScreenSpy, platformReadySpy, platformSpy, modalSpy, modalCtrlSpy, mockPlatform, mockBackButton;
  let base64Spy, fileSpy, cameraSpy, parseSpy, timerSpy;
  beforeEach(async(() => {
    statusBarSpy = jasmine.createSpyObj('StatusBar', ['styleDefault']);
    splashScreenSpy = jasmine.createSpyObj('SplashScreen', ['hide']);
    base64Spy = jasmine.createSpyObj('Base64', ['encodeFile']);
    fileSpy = jasmine.createSpyObj('File', ['readAsDataURL']);
    cameraSpy = jasmine.createSpyObj('CameraService', ['getMediaDefault']);
    parseSpy = jasmine.createSpyObj('ParseService', ['getList']);
    timerSpy = jasmine.createSpyObj('TimerService', ['startTimer']);
    platformReadySpy = jasmine.createSpy().and.returnValue(Promise.resolve());
    mockPlatform = new MockPlatform();
    mockPlatform.ready = platformReadySpy;
    mockBackButton = new MockBackButton();
    mockBackButton.subscribeWithPriority = jasmine.createSpy('subscribeWithPriority', (priority, fn) => {});
    mockPlatform.backButton = mockBackButton;
    platformSpy = jasmine.createSpyObj('Platform', { ready: platformReadySpy });
    modalSpy = jasmine.createSpyObj('Modal', ['present']);
    modalCtrlSpy = jasmine.createSpyObj('ModalController', ['create']);
    modalCtrlSpy.create.and.callFake(function () {
        return modalSpy;
    });
    TestBed.configureTestingModule({
      declarations: [AppComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [
        { provide: StatusBar, useValue: statusBarSpy },
        { provide: SplashScreen, useValue: splashScreenSpy },
        { provide: Platform, useValue: mockPlatform },
        {
          provide: ModalController,
          useValue: modalCtrlSpy
        },
        {provide: Base64, useValue: base64Spy},
        {provide: File, useValue: fileSpy},
        {provide: CameraService, useValue: cameraSpy},
        {provide: ParseService, useValue: parseSpy},
        {provide: TimerService, useValue: timerSpy},
      ],
      imports: [RouterTestingModule]
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it('should initialize the app', async () => {
    TestBed.createComponent(AppComponent);
    await platformReadySpy;
    expect(statusBarSpy.styleDefault).toHaveBeenCalled();
  });
});

