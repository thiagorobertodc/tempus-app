import {FormGroup} from '@angular/forms';

export class ValidatePassword {
    static checkPasswords(group: FormGroup) { // here we have the 'passwords' group
        let pass = group.get('password').value;
        let confirmPass = group.get('confirmPassword').value;

        if (pass !== confirmPass) {
            group.get('confirmPassword').setErrors({
                matchPassword: true
            });
        } else {
            return null;
        }
    }

}
