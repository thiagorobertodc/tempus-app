import {Component, OnInit, ViewChild} from '@angular/core';
import {UtilsService} from '../../services/utils.service';
import {FormBuilder, Validators} from '@angular/forms';
import {ValidatePassword} from './validate-password';
import {ValidationService} from '../../services/validation.service';
import {IonInput, NavController} from '@ionic/angular';
import {ParseService} from "../../services/parse.service";
import { TimerService } from 'src/app/services/timer.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  @ViewChild('c', {static: false}) public c: any;
  public form: any;
  public wait: boolean;
    public validationMessages = {
      email: [
        {type: 'required', message: 'Preencha seu e-mail'},
        {type: 'pattern', message: 'Este e-mail não está correto. Preencha um email válido'}
      ],

    password: [
      {type: 'required', message: 'Preencha sua senha'},
    ],
    name: [
      {type: 'required', message: 'Preencha seu nome'},
    ],
    address: [
      {type: 'required', message: 'Preencha seu endereco'},
    ],
    phone: [
      {type: 'required', message: 'Preencha seu telefone'},
    ],
    confirmPassword: [
      {type: 'required', message: 'Confirme sua senha'},
      {type: 'matchPassword', message: 'As senhas não combinam'}
    ]
  };

  constructor(public utils: UtilsService,
              public fb: FormBuilder,
              public formValidator: ValidationService,
              public navCtrl: NavController,
              public parse: ParseService,
              public timer: TimerService) {
  }

  ngOnInit() {
    this.form = this.fb.group({
      email: ['', Validators.compose([Validators.required, Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')])],
      password: ['', Validators.required],
      confirmPassword: ['', Validators.required],
      name: ['', Validators.required],
      username: ['']
    }, {validator: ValidatePassword.checkPasswords});

  }

  async checkData() {
    await this.utils.showLoading();
    this.wait = true;
    this.form.controls.username.setValue(this.form.get('email').value);
    if (this.formValidator.formValidation(this.form)) {
      if (this.form.get('password').value === this.form.get('confirmPassword').value) {
        this.signUp();
      } else {
        this.utils.showToaster('As senhas não conferem!', 3000);
        this.moveFocus(this.c);
      }
    }
    this.wait = false;
    this.utils.dismissLoading();
  }
  /** cadastra um novo usuário **/
  signUp() {

    const formSignUp = {
      name: this.form.get('name').value,
      email: this.form.get('email').value,
      password: this.form.get('password').value,
      username: this.form.get('email').value
    };
    this.wait = true;

    this.parse.signUp(formSignUp).then(user => {
      user.save().then(async r => {
        this.utils.showToaster('Cadastro realizado com sucesso!', 4000);
        await this.timer.stopTimer(1);
        this.navCtrl.navigateRoot('/tabs', {replaceUrl: true}).then(() => {
          this.wait = false;
          this.utils.dismissLoading();

        });
      }).catch(e => {
        this.utils.parseError(e);
      });
    }).catch(e => {
      this.wait = false;
      this.utils.parseError(e);
    });
  }

  moveFocus(nextElement: IonInput) {
    nextElement.setFocus();
  }
}



