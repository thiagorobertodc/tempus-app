import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ParseService } from '../../services/parse.service';
import { UtilsService } from '../../services/utils.service';
import {EventsService} from "../../services/events.service";
import { NavController } from '@ionic/angular';
@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.page.html',
  styleUrls: ['./add-user.page.scss'],
})
export class AddUserPage implements OnInit {
  public form: any;
  public addedUsers: any = [];
  public validationMessages = {
    email: [
      {type: 'required', message: 'Preencha seu e-mail'},
      {type: 'pattern', message: 'Este e-mail não está correto. Preencha um email válido'},
    ],
  }
  constructor(public parse: ParseService,
    public utils: UtilsService,
    public navCtrl: NavController,
    public events: EventsService) { }

  async ngOnInit() {
    this.form = new FormGroup({
      email: new FormControl('', Validators.compose([
          Validators.required,
          Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ]))
  });
    const user = this.parse.currentUser;
    this.addedUsers = await this.parse.getRelation(user, 'following'); 
   
  }

  deleteUser(userToRemove: any, index: any) {
    const user = this.parse.currentUser;
    this.utils.showAlert('Deseja deletar o usuário?',
    'Você não poderá mais ver os objetos registrados por ele.').then(resp => {
      if (resp.role !== 'cancel') {
        this.parse.removeRelation(user, 'following', userToRemove).then(() => {
          this.addedUsers.splice(index, 1);
          this.events.publish({updateUsersObjects: true});
        });
      }
  });
  }

  async add() {
    await this.utils.showLoading();
    const user = this.parse.currentUser;

    if (user) {
      if (this.form.get('email').value != '') {
        const userToAdd = await this.parse.getWhere('User', [['username', '=', this.form.get('email').value]]);
        console.log(userToAdd);
        if (userToAdd == null) {
          this.utils.dismissLoading();
          this.utils.showToaster('Email não encontrado!', 3000);
        } else {
          console.log(userToAdd.get('name'));
        this.parse.putRelation(user, 'following', userToAdd).then(async () => {
          this.utils.dismissLoading();
          this.events.publish({updateFavorites: true});
          
        this.utils.showToaster('Usuário adicionado!', 3000);
        }).catch(e => {
          this.utils.dismissLoading();
          this.utils.parseError(e);
        });
        }
      } else {
        this.utils.dismissLoading();
        this.utils.showToaster('Forneça um email válido', 3000);
    }
  } else {
    this.utils.dismissLoading();
        this.navCtrl.navigateRoot('/login');
  }

}
}
