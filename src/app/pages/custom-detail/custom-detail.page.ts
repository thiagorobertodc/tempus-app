import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController } from '@ionic/angular';
import { EventsService } from 'src/app/services/events.service';
import { ParseService } from 'src/app/services/parse.service';
import { TimerService } from 'src/app/services/timer.service';
import { UtilsService } from 'src/app/services/utils.service';

@Component({
  selector: 'app-custom-detail',
  templateUrl: './custom-detail.page.html',
  styleUrls: ['./custom-detail.page.scss'],
})
export class CustomDetailPage implements OnInit {
  public custom: any;
  public addedPhoto: any;
  public loading: any;
  public favCustom = false;
  constructor(public route: ActivatedRoute,
              public parse: ParseService,
              public utils: UtilsService,
              public events: EventsService,
              public navCtrl: NavController,
              public timer: TimerService) { }

              async ngOnInit() {
                this.loading = true;
                try {
                await this.utils.showLoading();
                const customId = this.route.snapshot.paramMap.get('id');
                this.parse.getItem('CustomObjects', customId).then(result => {
                  this.custom = result;
                  this.addedPhoto = result.get('photo').url();
                  const user = this.parse.currentUser;
                  const relation = user.relation('likedCustoms');
                  relation.query().find().then(async resp => {
                    console.log(resp);
                    for (const custom of resp) {
                      console.log('procurando');
                        if (custom.id === result.id) {
                            console.log('iguais');
                            this.favCustom = true;
                        }
                    }
                    this.utils.dismissLoading();
                    await this.timer.stopTimer(6);
                    this.loading = false;
                  });
                }).catch(error => {
                  this.utils.dismissLoading();
                  this.loading = false;
                  this.utils.showToaster('Erro ao carregar detalhes do objeto. Tente novamente.', 3000);
                  this.navCtrl.navigateBack('/tabs/tab1');
                });
                } catch {
                  this.utils.dismissLoading();
                  this.loading = false;
                  this.utils.showToaster('Erro ao carregar detalhes do objeto. Tente novamente.', 3000);
                  this.navCtrl.navigateBack('/tabs/tab1');
                }
            
              }

              favorite(custom: any, favCustom: boolean) {
                const user = this.parse.currentUser;
            
                if (user) {
                  if (!favCustom) {
                    this.parse.putRelation(user, 'likedCustoms', custom).then(async () => {
                      await this.utils.dismissLoading();
                      await this.timer.stopTimer(7);
                      this.favCustom = true;
                      this.events.publish({updateFavorites: true});
                    });
                  } else if (favCustom) {
                    this.parse.removeRelation(user, 'likedCustoms', custom).then(() => {
                      this.utils.dismissLoading();
                      this.favCustom = false;
                      this.events.publish({updateFavorites: true});
                    });
                  }
                } else {
                  this.navCtrl.navigateRoot('/login');
                }
              }

}
