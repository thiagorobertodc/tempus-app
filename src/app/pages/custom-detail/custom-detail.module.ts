import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CustomDetailPageRoutingModule } from './custom-detail-routing.module';

import { CustomDetailPage } from './custom-detail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CustomDetailPageRoutingModule
  ],
  declarations: [CustomDetailPage]
})
export class CustomDetailPageModule {}
