import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CustomDetailPage } from './custom-detail.page';

const routes: Routes = [
  {
    path: '',
    component: CustomDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CustomDetailPageRoutingModule {}
