import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CustomDetailPage } from './custom-detail.page';

describe('CustomDetailPage', () => {
  let component: CustomDetailPage;
  let fixture: ComponentFixture<CustomDetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomDetailPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CustomDetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));


});
