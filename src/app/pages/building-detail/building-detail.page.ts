import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {ParseService} from "../../services/parse.service";
import {UtilsService} from "../../services/utils.service";
import {EventsService} from "../../services/events.service";
import {NavController} from "@ionic/angular";
import { TimerService } from 'src/app/services/timer.service';

@Component({
  selector: 'app-building-detail',
  templateUrl: './building-detail.page.html',
  styleUrls: ['./building-detail.page.scss'],
})
export class BuildingDetailPage implements OnInit {
  public building: any;
  public addedPhoto: any;
  public loading: any;
  public favBuilding = false;
  constructor(public route: ActivatedRoute,
              public parse: ParseService,
              public utils: UtilsService,
              public events: EventsService,
              public navCtrl: NavController,
              public timer: TimerService) { }

  async ngOnInit() {
    this.loading = true;
    try {
      await this.utils.showLoading();
      const buildId = this.route.snapshot.paramMap.get('id');
      this.parse.getItem('RegisteredBuildings', buildId).then(result => {
      this.building = result;
      this.addedPhoto = result.get('photo').url();
      const user = this.parse.currentUser;
      const relation = user.relation('likedBuildings');
      relation.query().find().then(async resp => {
        console.log(resp);
        for (const building of resp) {
          if (building.id === result.id) {
            this.favBuilding = true;
          }
        }
        this.loading = false;
        this.utils.dismissLoading();
        await this.timer.stopTimer(6);
        this.timer.startTimer();
      });
    }).catch(error => {
      this.utils.dismissLoading();
      this.loading = false;
      this.utils.showToaster('Erro ao carregar detalhes do edifício. Tente novamente.', 3000);
      this.navCtrl.navigateBack('/tabs/tab1');
    });
    } catch {
      this.utils.dismissLoading();
      this.loading = false;
      this.utils.showToaster('Erro ao carregar detalhes do edifício. Tente novamente.', 3000);
      this.navCtrl.navigateBack('/tabs/tab1');
    }
}

  favorite(building: any, favBuilding: boolean) {
    const user = this.parse.currentUser;

    if (user) {
      if (!favBuilding) {
        this.parse.putRelation(user, 'likedBuildings', building).then(async () => {
          await this.utils.dismissLoading();
          this.favBuilding = true;
          await this.timer.stopTimer(7);
          this.events.publish({updateFavorites: true});
        });
      } else if (favBuilding) {
        this.parse.removeRelation(user, 'likedBuildings', building).then(() => {
          this.utils.dismissLoading();
          this.favBuilding = false;
          this.events.publish({updateFavorites: true});
        });
      }
    } else {
      this.navCtrl.navigateRoot('/login');
    }
  }
}
