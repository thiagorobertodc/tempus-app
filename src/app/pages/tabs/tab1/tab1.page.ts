import { Component } from '@angular/core';
import {MenuController, NavController} from '@ionic/angular';
import { TimerService } from 'src/app/services/timer.service';
import {ParseService} from "../../../services/parse.service";
import {UtilsService} from "../../../services/utils.service";
@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  public animals: any = [];
  public trees: any = [];
  public bugs: any = [];
  public reliefs: any = [];
  public buildings: any = [];
  public customObjects: any = [];
  public loading: any = true;
  constructor(public menu: MenuController,
              public parse: ParseService,
              public utils: UtilsService,
              public navController: NavController,
              public menuCtrl: MenuController,
              public timer: TimerService) {}

  async ngOnInit() {
    await this.utils.showLoading();
    this.loading = true;
    let user = this.parse.currentUser;
    this.animals = await this.parse.getList('RegisteredAnimals', [['user', '=', user]],'', '', '', '', '*');
    this.trees = await this.parse.getList('RegisteredTrees', [['user', '=', user]]);
    this.buildings = await this.parse.getList('RegisteredBuildings', [['user', '=', user]]);
    this.bugs = await this.parse.getList('RegisteredInsects', [['user', '=', user]]);
    this.reliefs = await this.parse.getList('RegisteredReliefs', [['user', '=', user]], '', '', '', '', '*');
    this.customObjects = await this.parse.getList('CustomObjects', [['user', '=', user]]);
    console.log(this.customObjects);
    console.log(this.animals);
    console.log(this.trees);
    console.log(this.buildings);
    console.log(this.reliefs);
    console.log(this.bugs);
    this.loading = false;
    this.utils.dismissLoading();
  }

  async ionViewWillEnter() {
    await this.menuCtrl.enable(true);
  }

  async ionViewWillLeave() {
    await this.menuCtrl.enable(false);
  }
  openFirst() {
    this.menu.enable(true, 'first');
    this.menu.open('first');
  }

  openEnd() {
    this.menu.open('end');
  }

  openCustom() {
    this.menu.enable(true, 'custom');
    this.menu.open('custom');
  }

  async animalDetail(animalId) {
  await this.navController.navigateForward('/animal-detail/' + animalId);
}

async buildDetail(buildId) {
    await this.navController.navigateForward('/building-detail/' + buildId);
}

async bugDetail(bugId) {
    await this.navController.navigateForward('/insect-detail/' + bugId);
}

  async reliefDetail(reliefId) {
    await this.navController.navigateForward('/relief-detail/' + reliefId);
  }

  async treeDetail(treeId) {
    await this.navController.navigateForward('/tree-detail/' + treeId);
  }

  async customDetail(customId) {
    await this.navController.navigateForward('/custom-detail/' + customId);
  }

  deleteAnimal(animal: any, index: any) {
    this.utils.showAlert('Deseja deletar o animal?',
        'Você perderá os dados registrados.').then(resp => {
        if (resp.role !== 'cancel') {
            this.parse.deleteItem(animal).then(async r => {
                    this.animals.splice(index, 1);
                    this.utils.showToaster('Animal removido com sucesso', 3000);
                    await this.timer.stopTimer(8);
                }
            );
        }
    });
}

deleteTree(tree: any, index: any) {
  this.utils.showAlert('Deseja deletar a árvore?',
      'Você perderá os dados registrados.').then(resp => {
      if (resp.role !== 'cancel') {
          this.parse.deleteItem(tree).then(async r => {
                  this.trees.splice(index, 1);
                  this.utils.showToaster('Árvore removida com sucesso', 3000);
                  await this.timer.stopTimer(8);
              }
          );
      }
  });
}

deleteInsect(insect: any, index: any) {
  this.utils.showAlert('Deseja deletar o inseto?',
      'Você perderá os dados registrados.').then(resp => {
      if (resp.role !== 'cancel') {
          this.parse.deleteItem(insect).then(async r => {
                  this.bugs.splice(index, 1);
                  this.utils.showToaster('Inseto removido com sucesso', 3000);
                  await this.timer.stopTimer(8);
              }
          );
      }
  });
}

deleteBuilding(building: any, index: any) {
  this.utils.showAlert('Deseja deletar o edifício?',
      'Você perderá os dados registrados.').then(resp => {
      if (resp.role !== 'cancel') {
          this.parse.deleteItem(building).then(async r => {
                  this.buildings.splice(index, 1);
                  this.utils.showToaster('Edifício removido com sucesso', 3000);
                  await this.timer.stopTimer(8);
              }
          );
      }
  });
}

deleteRelief(relief: any, index: any) {
  this.utils.showAlert('Deseja deletar o relevo?',
      'Você perderá os dados registrados.').then(resp => {
      if (resp.role !== 'cancel') {
          this.parse.deleteItem(relief).then(async r => {
                  this.reliefs.splice(index, 1);
                  this.utils.showToaster('Relevo removido com sucesso', 3000);
                  await this.timer.stopTimer(8);
              }
          );
      }
  });
}

deleteCustom(custom: any, index: any) {
  this.utils.showAlert('Deseja deletar o objeto personalizado?',
      'Você perderá os dados registrados.').then(resp => {
      if (resp.role !== 'cancel') {
          this.parse.deleteItem(custom).then(async r => {
                  this.customObjects.splice(index, 1);
                  this.utils.showToaster('Objeto removido com sucesso', 3000);
                  await this.timer.stopTimer(8);
              }
          );
      }
  });
}
}
