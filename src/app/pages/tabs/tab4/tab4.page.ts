import { Component, OnInit } from '@angular/core';
import { TimerService } from 'src/app/services/timer.service';
import {ParseService} from "../../../services/parse.service";
import {UtilsService} from "../../../services/utils.service";

@Component({
  selector: 'app-tab4',
  templateUrl: './tab4.page.html',
  styleUrls: ['./tab4.page.scss'],
})
export class Tab4Page implements OnInit {
  public animals: any = [];
  public trees: any = [];
  public bugs: any = [];
  public reliefs: any = [];
  public buildings: any = [];
  public custom: any = [];
  constructor(
      public parse: ParseService,
      public utils: UtilsService,
      public timer: TimerService,
  ) { }

  async ngOnInit() {
    await this.utils.showLoading();
    await this.timer.stopTimer(9);
    let user = this.parse.currentUser;
    this.animals = await this.parse.getList('RegisteredAnimals', [['user', '=', user]],'createdAt', 'true', '', 5, '');
    this.trees = await this.parse.getList('RegisteredTrees', [['user', '=', user]], 'createdAt', 'true', '', 5);
    this.buildings = await this.parse.getList('RegisteredBuildings', [['user', '=', user]], 'createdAt', 'true', '', 5);
    this.bugs = await this.parse.getList('RegisteredInsects', [['user', '=', user]], 'createdAt', 'true', '', 5);
    this.reliefs = await this.parse.getList('RegisteredReliefs', [['user', '=', user]], 'createdAt', 'true', '', 5, '');
    this.custom = await this.parse.getList('CustomObjects',  [['user', '=', user]], 'createdAt', 'true', '', 5, '');
    this.utils.dismissLoading();
  }

  async ionViewWillEnter() {
    let user = this.parse.currentUser;
    this.animals = await this.parse.getList('RegisteredAnimals', [['user', '=', user]],'createdAt', 'true', '', 5, '');
    this.trees = await this.parse.getList('RegisteredTrees', [['user', '=', user]], 'createdAt', 'true', '', 5);
    this.buildings = await this.parse.getList('RegisteredBuildings', [['user', '=', user]], 'createdAt', 'true', '', 5);
    this.bugs = await this.parse.getList('RegisteredInsects', [['user', '=', user]], 'createdAt', 'true', '', 5);
    this.reliefs = await this.parse.getList('RegisteredReliefs', [['user', '=', user]], 'createdAt', 'true', '', 5, '');
    this.custom = await this.parse.getList('CustomObjects',  [['user', '=', user]], 'createdAt', 'true', '', 5, '');
    
  }

}
