import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import {RouterTestingModule} from "@angular/router/testing";
import { Tab4Page } from './tab4.page';
import { MockParse, MockTimer } from 'src/mocks';
import { TimerService } from 'src/app/services/timer.service';
import { ParseService } from 'src/app/services/parse.service';


describe('Tab4Page', () => {
  let component: Tab4Page;
  let fixture: ComponentFixture<Tab4Page>;
  let timerStopSpy, mockTimer, parseSpy, getListSpy;
  
  beforeEach(async(() => {
    timerStopSpy = jasmine.createSpy().and.returnValue(Promise.resolve());
    mockTimer = new MockTimer();
    mockTimer.stopTimer = timerStopSpy;
    getListSpy = jasmine.createSpy().and.returnValue(Promise.resolve());
    parseSpy = new MockParse();
    parseSpy.getList = getListSpy;
    TestBed.configureTestingModule({
      declarations: [ Tab4Page ],
      imports: [IonicModule.forRoot(), RouterTestingModule],
      providers: [
        {provide: TimerService, useValue: mockTimer},
        {provide: ParseService, useValue: parseSpy},
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(Tab4Page);
    component = fixture.componentInstance;
    component.animals = [];
    component.bugs = [];
    component.buildings = [];
    component.custom = [];
    component.reliefs = [];
    component.trees = [];
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
