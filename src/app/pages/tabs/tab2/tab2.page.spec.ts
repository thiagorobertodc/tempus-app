import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import {RouterTestingModule} from "@angular/router/testing";
import { MockParse, MockTimer } from 'src/mocks';
import { TimerService } from 'src/app/services/timer.service';
import { ParseService } from 'src/app/services/parse.service';
import { Tab2Page } from './tab2.page';



describe('Tab2Page', () => {
  let component: Tab2Page;
  let fixture: ComponentFixture<Tab2Page>;
  let timerStopSpy, mockTimer, parseSpy, getListSpy;
  
  beforeEach(async(() => {
    timerStopSpy = jasmine.createSpy().and.returnValue(Promise.resolve());
    mockTimer = new MockTimer();
    mockTimer.stopTimer = timerStopSpy;
    getListSpy = jasmine.createSpy().and.returnValue(Promise.resolve());
    parseSpy = new MockParse();
    parseSpy.getList = getListSpy;
    TestBed.configureTestingModule({
      declarations: [ Tab2Page ],
      imports: [IonicModule.forRoot(), RouterTestingModule],
      providers: [
        {provide: TimerService, useValue: mockTimer},
        {provide: ParseService, useValue: parseSpy},
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(Tab2Page);
    component = fixture.componentInstance;
    component.favAnimals = [];
    component.favInsects = [];
    component.favCustoms = [];
    component.favReliefs = [];
    component.favTrees = [];
    component.favBuildings = [];
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
