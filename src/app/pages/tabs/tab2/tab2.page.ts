import {Component, ViewChild} from '@angular/core';
import {ParseService} from "../../../services/parse.service";
import {EventsService} from "../../../services/events.service";
import {UtilsService} from "../../../services/utils.service";
import { TimerService } from 'src/app/services/timer.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
  public favAnimals = [];
  public favBuildings = [];
  public favTrees = [];
  public favInsects = [];
  public favReliefs = [];
  public favCustoms = [];
  @ViewChild('content', {static: false}) public content: any;
  public loaded = false;
  constructor(public parse: ParseService,
              public events: EventsService,
              public utils: UtilsService,
              public timer: TimerService) {
    this.events.getObservable().subscribe((data) => {

      if (data.updateFavorites) {
        this.loadAll();
      }
    });
  }

  async ionViewDidEnter() {
    await this.content.scrollToPoint(0, 0, 300);
    await this.timer.stopTimer(10);
  }

/*  unlikeAnimal(animal: any) {
    for (const an of this.favAnimals) {
      if (an.id === animal.id) {
        const user = this.parse.currentUser;
        this.parse.removeRelation(user, 'likedAnimals', animal).then(() => {
          this.favAnimals = this.favAnimals.filter(f => f !== animal);
          this.loadAll();
        });
        return;
      }
    }
  } */

 /* unlikeBuilding(building: any) {
    for (const build of this.favBuildings) {
      if (build.id === building.id) {
        const user = this.parse.currentUser;
        this.parse.removeRelation(user, 'likedBuildings', building).then(() => {
          this.favAnimals = this.favAnimals.filter(f => f !== building);
          this.loadAll();
        });
        return;
      }
    }
  } */

  ngOnInit() {
    this.loaded = false;
    this.loadAll();
  }

  async loadAll() {
    const user = this.parse.currentUser;

    if (user) {
      await this.utils.showLoading();
      await this.loadAnimals();
      await this.loadBuildings();
      await this.loadTrees();
      await this.loadInsects();
      await this.loadReliefs();
      await this.loadCustoms();
      this.loaded = true;
      this.utils.dismissLoading();
    } else {
      this.loaded = true;
    }
  }

  async loadAnimals() {
    const user = this.parse.currentUser;
    const relation = user.relation('likedAnimals');
    relation.query().find().then(r => {
      if (r !== null) {
        this.favAnimals = r;
      }
    }).catch(e => {
      this.loaded = true;
      this.utils.parseError(e);
    });
  }

  async loadBuildings() {
    const user = this.parse.currentUser;
    const relation = user.relation('likedBuildings');
    relation.query().find().then(r => {
      if (r !== null) {
        this.favBuildings = r;
      }
    }).catch(e => {
      this.loaded = true;
      this.utils.parseError(e);
    });
  }

  async loadTrees() {
    const user = this.parse.currentUser;
    const relation = user.relation('likedTrees');
    relation.query().find().then(r => {
      if (r !== null) {
        this.favTrees = r;
      }
    }).catch(e => {
      this.loaded = true;
      this.utils.parseError(e);
    });
  }

  async loadInsects() {
    const user = this.parse.currentUser;
    const relation = user.relation('likedInsects');
    relation.query().find().then(r => {
      if (r !== null) {
        this.favInsects = r;
      }
    }).catch(e => {
      this.loaded = true;
      this.utils.parseError(e);
    });
  }

  async loadReliefs() {
    const user = this.parse.currentUser;
    const relation = user.relation('likedReliefs');
    relation.query().find().then(r => {
      if (r !== null) {
        this.favReliefs = r;
      }
    }).catch(e => {
      this.loaded = true;
      this.utils.parseError(e);
    });
  }

  async loadCustoms() {
    const user = this.parse.currentUser;
    const relation = user.relation('likedCustoms');
    relation.query().find().then(r => {
      if (r !== null) {
        this.favCustoms = r;
      }
    }).catch(e => {
      this.loaded = true;
      this.utils.parseError(e);
    });
  }
}
