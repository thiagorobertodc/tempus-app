import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import {RouterTestingModule} from "@angular/router/testing";
import { Tab3Page } from './tab3.page';
class MockTimer {
  stopTimer: jasmine.Spy<any>;
}

class MockCustomObject {
  get: jasmine.Spy<any>;
}
describe('Tab3Page', () => {
  let component: Tab3Page;
  let fixture: ComponentFixture<Tab3Page>;
  let timerStopSpy, mockTimer, ccGetSpy, customObjectSpy;
  beforeEach(async(() => {
    timerStopSpy = jasmine.createSpy().and.returnValue(Promise.resolve());
    ccGetSpy = jasmine.createSpy().and.returnValue({
      'name': 'Custom_Name'
    });
    customObjectSpy = new MockCustomObject();
    customObjectSpy.get = ccGetSpy;
    mockTimer = new MockTimer();
    mockTimer.stopTimer = timerStopSpy;
    TestBed.configureTestingModule({
      declarations: [Tab3Page],
      imports: [IonicModule.forRoot(), RouterTestingModule]
    }).compileComponents();

    fixture = TestBed.createComponent(Tab3Page);
    component = fixture.componentInstance;
    component.user = customObjectSpy;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
