import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
import { TimerService } from 'src/app/services/timer.service';
import { UtilsService } from 'src/app/services/utils.service';
import {ParseService} from "../../../services/parse.service";

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {
  public user: any;
  constructor(public parse: ParseService,
              public navCtrl: NavController,
              public utils: UtilsService,
              public timer: TimerService) {
    this.user = this.parse.currentUser;
  }
  

  async logOut() {
    await this.utils.showLoading();
    this.parse.logOut().then(async () => {
      this.utils.dismissLoading();
      this.navCtrl.navigateRoot('/login');
    });
  }

  goToPage() {
    this.navCtrl.navigateForward('/about');
  }

  addUser() {
    this.navCtrl.navigateForward('add-user');
  }

}
