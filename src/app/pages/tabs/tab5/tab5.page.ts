import { Component, OnInit } from '@angular/core';
import {EventsService} from "../../../services/events.service";
import { UtilsService } from '../../../services/utils.service';
import { ParseService } from '../../../services/parse.service';
import { NavController } from '@ionic/angular';
@Component({
  selector: 'app-tab5',
  templateUrl: './tab5.page.html',
  styleUrls: ['./tab5.page.scss'],
})
export class Tab5Page implements OnInit {
  public loading: any = true;
  public objects: any = [];
  public animals: any = [];
  public loaded = false;
  constructor(public events: EventsService,
    public utils: UtilsService,
    public parse: ParseService,
    public navController: NavController) {
    this.events.getObservable().subscribe((data) => {

      if (data.updateUsersObjects) {
        this.loadAll();
      }
    });
  }

  ngOnInit() {
    this.loaded = false;
    this.loadAll();
  }

  async loadAll() {
    const user = this.parse.currentUser;

    if (user) {
      await this.utils.showLoading();
      await this.loadObjects();
      this.loaded = true;
      this.utils.dismissLoading();
    } else {
      this.loaded = true;
    }
  }

  async loadObjects() {
    this.objects = [];
    const user = this.parse.currentUser;
    let r = await this.parse.getRelation(user, 'following'); 
    r.forEach(async element => {
      let userAnimals = await this.parse.getList('RegisteredAnimals', [['user', '=', element]]);
      userAnimals.forEach(elementAnimal => {
        this.objects.push(elementAnimal);
      });

      let userBuildings = await this.parse.getList('RegisteredBuildings', [['user', '=', element]]);
      userBuildings.forEach(elementBuilding => {
        this.objects.push(elementBuilding);
      });

      let userTrees = await this.parse.getList('RegisteredTrees', [['user', '=', element]]);
      userTrees.forEach(elementTree => {
        this.objects.push(elementTree);
      });

      let userInsects = await this.parse.getList('RegisteredInsects', [['user', '=', element]]);
      userInsects.forEach(elementInsect => {
        this.objects.push(elementInsect);
      });

      let userReliefs = await this.parse.getList('RegisteredReliefs', [['user', '=', element]]);
      userReliefs.forEach(elementRelief => {
        this.objects.push(elementRelief);
      });

      let userCustom = await this.parse.getList('CustomObjects', [['user', '=', element]]);
      userCustom.forEach(userC => {
        this.objects.push(userC);
      });
      
      this.utils.dismissLoading();
    });
  }

  async animalDetail(animalId) {
    await this.navController.navigateForward('/animal-detail/' + animalId);
  }
  
  async buildDetail(buildId) {
      await this.navController.navigateForward('/building-detail/' + buildId);
  }
  
  async bugDetail(bugId) {
      await this.navController.navigateForward('/insect-detail/' + bugId);
  }
  
    async reliefDetail(reliefId) {
      await this.navController.navigateForward('/relief-detail/' + reliefId);
    }
  
    async treeDetail(treeId) {
      await this.navController.navigateForward('/tree-detail/' + treeId);
    }
  
    async customDetail(customId) {
      await this.navController.navigateForward('/custom-detail/' + customId);
    }
  

}
