import {FormGroup} from '@angular/forms';

export class ValidateEmail {
    static checkPasswords(group: FormGroup) { // here we have the 'passwords' group
        let pass = group.get('email').value;
        let confirmPass = group.get('confirmEmail').value;

        if (pass !== confirmPass) {
            group.get('confirmEmail').setErrors({
                matchPassword: true
            });
        } else {
            return null;
        }
    }

}
