import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {UtilsService} from "../../services/utils.service";
import { TimerService } from 'src/app/services/timer.service';
import { ParseService } from 'src/app/services/parse.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.page.html',
  styleUrls: ['./forgot-password.page.scss'],
})
export class ForgotPasswordPage implements OnInit {
  public form: any;
  public validationMessages = {
    email: [
      {type: 'required', message: 'Preencha seu e-mail'},
      {type: 'pattern', message: 'Este e-mail não está correto. Preencha um email válido'},
    ],
  }
  constructor(public fb: FormBuilder,
              public utils: UtilsService,
              public parse: ParseService,
              public timer: TimerService) { }

  async ngOnInit() {
    this.form = new FormGroup({
      email: new FormControl('', Validators.compose([
          Validators.required,
          Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ]))
  });
}

async resetPassword() {
  if (this.form.get('email').value != '') {
      await this.parse.resetPass(this.form.get('email').value);
      this.utils.showToaster('Email de recuperação enviado.', 3000);
  } else {
      this.utils.showToaster('Forneça um email válido', 3000);
  }
}

}