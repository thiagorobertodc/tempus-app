import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import {RouterTestingModule} from "@angular/router/testing";
import { TimerService } from 'src/app/services/timer.service';
import { ForgotPasswordPage } from './forgot-password.page';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
describe('ForgotPasswordPage', () => {
  let component: ForgotPasswordPage;
  let fixture: ComponentFixture<ForgotPasswordPage>;
  let timerSpy;
  beforeEach(async(() => {
    timerSpy = jasmine.createSpyObj('TimerService', ['startTimer']);
    TestBed.configureTestingModule({
      declarations: [ ForgotPasswordPage ],
      imports: [IonicModule.forRoot(),  RouterTestingModule, FormsModule, ReactiveFormsModule,]
    }).compileComponents();

    fixture = TestBed.createComponent(ForgotPasswordPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
