import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { MockParse, MockTimer } from 'src/mocks';
import {RouterTestingModule} from "@angular/router/testing";
import { TreeDetailPage } from './tree-detail.page';
import { TimerService } from 'src/app/services/timer.service';
import { ParseService } from 'src/app/services/parse.service';

class MockCustomTree {
  get: jasmine.Spy<any>;
}

describe('TreeDetailPage', () => {
  let component: TreeDetailPage;
  let fixture: ComponentFixture<TreeDetailPage>;
  let timerStopSpy, startTimerSpy, mockTimer, parseSpy, getItemSpy, treeGetSpy, treeGetDateSpy, treeSpy;
  beforeEach(async(() => {
    treeGetDateSpy = jasmine.createSpy().and.returnValue(new Date());
    treeGetSpy = jasmine.createSpy().and.returnValue({
      'name': 'any_name',
      'location': 'any_location',
      'createdAt': treeGetDateSpy,
      'topShape': 'any_shape',
      'leafShape': 'any_shape',
      'fruitful': new Boolean(),
    });
    
    treeSpy = new MockCustomTree;
    treeSpy.get = treeGetSpy;
    timerStopSpy = jasmine.createSpy().and.returnValue(Promise.resolve());
    startTimerSpy = jasmine.createSpy().and.returnValue(Promise.resolve());
    mockTimer = new MockTimer();
    mockTimer.stopTimer = timerStopSpy;
    mockTimer.startTimer = timerStopSpy;
    getItemSpy = jasmine.createSpy().and.returnValue(Promise.resolve());
    parseSpy = new MockParse();
    parseSpy.getItem = getItemSpy;
    TestBed.configureTestingModule({
      declarations: [ TreeDetailPage ],
      imports: [IonicModule.forRoot(), RouterTestingModule],
      providers: [
        {provide: TimerService, useValue: mockTimer},
        {provide: ParseService, useValue: parseSpy},
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(TreeDetailPage);
    component = fixture.componentInstance;
    component.tree = treeSpy;
    fixture.detectChanges();
  }));

 
});
