import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import { NavController } from '@ionic/angular';
import {ParseService} from "../../services/parse.service";
import {UtilsService} from "../../services/utils.service";
import {EventsService} from "../../services/events.service";
import { TimerService } from 'src/app/services/timer.service';

@Component({
  selector: 'app-tree-detail',
  templateUrl: './tree-detail.page.html',
  styleUrls: ['./tree-detail.page.scss'],
})
export class TreeDetailPage implements OnInit {
  public tree: any;
  public addedPhoto: any;
  public loading;
  public favTree = false;
  constructor(public route: ActivatedRoute,
    public parse: ParseService,
    public utils: UtilsService,
    public events: EventsService,
    public navCtrl: NavController,
    public timer: TimerService) {
  }

  async ngOnInit() {
    this.loading = true;
    try {
    await this.utils.showLoading();
    const treeId = this.route.snapshot.paramMap.get('id');
    this.parse.getItem('RegisteredTrees', treeId).then(async result => {
      this.tree = result;
      this.addedPhoto = result.get('photo').url();
      this.loading = false;
      this.utils.dismissLoading();
      await this.timer.stopTimer(6);
      this.timer.startTimer();
    }).catch(error => {
      this.utils.dismissLoading();
      this.loading = false;
      this.utils.showToaster('Erro ao carregar detalhes da árvore. Tente novamente.', 3000);
      this.navCtrl.navigateBack('/tabs/tab1');
    });
    } catch {
      this.utils.dismissLoading();
      this.loading = false;
      this.utils.showToaster('Erro ao carregar detalhes da árvore. Tente novamente.', 3000);
      this.navCtrl.navigateBack('/tabs/tab1');
    }

  }

  favorite(tree: any, favTree: boolean) {
    const user = this.parse.currentUser;

    if (user) {
      if (!favTree) {
        this.parse.putRelation(user, 'likedTrees', tree).then(async () => {
          await this.utils.dismissLoading();
          this.favTree = true;
          await this.timer.stopTimer(7);
          this.events.publish({updateFavorites: true});
        });
      } else if (favTree) {
        this.parse.removeRelation(user, 'likedTrees', tree).then(() => {
          this.utils.dismissLoading();
          this.favTree = false;
          this.events.publish({updateFavorites: true});
        });
      }
    } else {
      this.navCtrl.navigateRoot('/login');
    }
  }
}
