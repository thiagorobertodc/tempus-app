import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule, NavController } from '@ionic/angular';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { LoginPage } from './login.page';
import {RouterTestingModule} from "@angular/router/testing";
import {By} from "@angular/platform-browser";
import {DebugElement} from "@angular/core";
import {ParseService} from "../../services/parse.service";
import {ParseMock} from "../../../mocks";
import * as faker from 'faker';
describe('LoginPage', () => {
  let component: LoginPage;
  let fixture: ComponentFixture<LoginPage>;
  let de: DebugElement;
  let el: HTMLElement;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginPage ],
      imports: [IonicModule.forRoot(), RouterTestingModule, FormsModule, ReactiveFormsModule,],
      providers: [
        {
          provide: LoginPage,
          useClass: ParseMock,
        }
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(LoginPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  afterEach(() => {
    fixture.destroy();
    component = null;
    de = null;
    el = null;
  });
  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have a form input', () => {
    expect(component.form).toBeTruthy();
  });

  it('should correctly render the passed Email Input value', () => {
    const email = faker.internet.email();
    fixture.debugElement.query(By.css('#email')).nativeElement.value = email;
    fixture.debugElement.query(By.css('#email')).nativeElement.dispatchEvent(new Event('input'));
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(component.form.get('email').value).toBe(email); // this pass
    });
  });

  it('should correctly render the Email error message when Input value is wrong', () => {
    fixture.debugElement.query(By.css('#email')).nativeElement.value = 'th';
    fixture.debugElement.query(By.css('#email')).nativeElement.dispatchEvent(new Event('input'));
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(fixture.debugElement.query(By.css('#email-error')).nativeElement.value).toBe('Este e-mail não está correto. Preencha um email válido'); // this pass
    });
  });

  it('should correctly render the Email error message when Input value is empty', () => {
    fixture.debugElement.query(By.css('#email')).nativeElement.value = 'th';
    fixture.debugElement.query(By.css('#email')).nativeElement.dispatchEvent(new Event('input'));
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(fixture.debugElement.query(By.css('#email-error')).nativeElement.value).toBe('Este e-mail não está correto. Preencha um email válido'); // this pass
    });
  });

  it('should correctly render the Password Input value', () => {
    const password = faker.internet.password();
    fixture.debugElement.query(By.css('#password')).nativeElement.value = password;
    fixture.debugElement.query(By.css('#password')).nativeElement.dispatchEvent(new Event('input'));
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(component.form.get('password').value).toBe(password); // this pass
    });
  });

  it('should correctly render the Password error message when Password is empty', () => {
    fixture.debugElement.query(By.css('#password')).nativeElement.value = '';
    fixture.debugElement.query(By.css('#password')).nativeElement.dispatchEvent(new Event('input'));
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(fixture.debugElement.query(By.css('#password-error')).nativeElement.value).toBe('Preencha sua senha'); // this pass
    });
  });


  it("should call parse Login Service on button click", () => {
    let parse = fixture.debugElement.injector.get(ParseService);
    spyOn(parse, "logIn");

    de = fixture.debugElement.query(By.css("ion-button"));

    de.triggerEventHandler("click", null);

    expect(parse.logIn).toHaveBeenCalled();
  });

  it("should call Navigation on register tag click", () => {
    let navCtrl = fixture.debugElement.injector.get(NavController);
    spyOn(navCtrl, "navigateForward");

    de = fixture.debugElement.query(By.css("#register-tag"));

    de.triggerEventHandler("click", null);

    expect(navCtrl.navigateForward).toHaveBeenCalled();
  });

  it("should call Navigation on about tag click", () => {
    let navCtrl = fixture.debugElement.injector.get(NavController);
    spyOn(navCtrl, "navigateForward");

    de = fixture.debugElement.query(By.css("#about-tag"));

    de.triggerEventHandler("click", null);

    expect(navCtrl.navigateForward).toHaveBeenCalled();
  });
});
