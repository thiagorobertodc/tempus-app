import {Component, OnInit, ViewChild} from '@angular/core';
import {NavController} from "@ionic/angular";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {ValidationService} from "../../services/validation.service";
import {ParseService} from "../../services/parse.service";
import {UtilsService} from "../../services/utils.service";
import { TimerService } from 'src/app/services/timer.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.page.html',
    styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
    public form: any;
    public user: any;
    public wait: boolean;
    @ViewChild('content', {static: false}) public content: any;
    public validationMessages = {
        email: [
            {type: 'required', message: 'Preencha seu e-mail'},
            {type: 'pattern', message: 'Este e-mail não está correto. Preencha um email válido'}
        ],

        password: [
            {type: 'required', message: 'Preencha sua senha'},
        ]
    };


    constructor(public navCtrl: NavController,
                public utils: UtilsService,
                public formValidator: ValidationService,
                public parse: ParseService,
                public timer: TimerService) {
    }
    async ionViewDidEnter() {
        await this.delay(2000);
        await this.content.scrollToBottom();
      }
    ngOnInit() {
        this.form = new FormGroup({
            email: new FormControl('', Validators.compose([
                Validators.required,
                Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
            ])),
            password: new FormControl('', Validators.required)
        });

    }

    checkData() {
        this.wait = true;
        this.enter();
        // if (this.formValidator.formValidation(this.form)) {
        //
        // }
    }


    enter() {
        this.parse.logIn(this.form.get('email').value, this.form.get('password').value).then(answer => {
            this.navCtrl.navigateRoot('/tabs/tab1');
        }).catch(error => {
            this.utils.showToaster(error.message);
        });
    }

    async goPage(url: string) {
        this.timer.startTimer();
        await this.navCtrl.navigateForward(url);
    }

     delay(ms: number) {
        return new Promise( resolve => setTimeout(resolve, ms) );
    }

    
}
