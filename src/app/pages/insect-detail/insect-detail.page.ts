import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import { NavController } from '@ionic/angular';
import {ParseService} from "../../services/parse.service";
import {UtilsService} from "../../services/utils.service";
import {EventsService} from "../../services/events.service";
import { TimerService } from 'src/app/services/timer.service';

@Component({
  selector: 'app-insect-detail',
  templateUrl: './insect-detail.page.html',
  styleUrls: ['./insect-detail.page.scss'],
})
export class InsectDetailPage implements OnInit {
  public insect: any;
  public addedPhoto: any;
  public loading: any;
  public favInsect: any;
  constructor(public route: ActivatedRoute,
    public parse: ParseService, 
    public utils: UtilsService,
    public events: EventsService,
    public navCtrl: NavController,
    public timer: TimerService) { }

  async ngOnInit() {
    this.loading = true;
    try {
      await this.utils.showLoading();
      const insectId = this.route.snapshot.paramMap.get('id');
      this.parse.getItem('RegisteredInsects', insectId).then(async result => {
        this.insect = result;
        this.addedPhoto = result.get('photo').url();
        this.utils.dismissLoading();
        await this.timer.stopTimer(6);
        this.timer.startTimer();
      }).catch(error => {
        this.utils.dismissLoading();
      this.loading = false;
      this.utils.showToaster('Erro ao carregar detalhes do inseto. Tente novamente.', 3000);
      this.navCtrl.navigateBack('/tabs/tab1');
      });
    } catch {
      this.utils.dismissLoading();
      this.loading = false;
      this.utils.showToaster('Erro ao carregar detalhes do inseto. Tente novamente.', 3000);
      this.navCtrl.navigateBack('/tabs/tab1');
    }
  }

  favorite(insect: any, favInsect: boolean) {
    const user = this.parse.currentUser;

    if (user) {
      if (!favInsect) {
        this.parse.putRelation(user, 'likedInsects', insect).then(async () => {
          await this.utils.dismissLoading();
          this.favInsect = true;
          await this.timer.stopTimer(7);
          this.events.publish({updateFavorites: true});
        });
      } else if (favInsect) {
        this.parse.removeRelation(user, 'likedInsects', insect).then(() => {
          this.utils.dismissLoading();
          this.favInsect = false;
          this.events.publish({updateFavorites: true});
        });
      }
    } else {
      this.navCtrl.navigateRoot('/login');
    }
  }

}
