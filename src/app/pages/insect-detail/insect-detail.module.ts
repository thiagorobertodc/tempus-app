import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InsectDetailPageRoutingModule } from './insect-detail-routing.module';

import { InsectDetailPage } from './insect-detail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InsectDetailPageRoutingModule
  ],
  declarations: [InsectDetailPage]
})
export class InsectDetailPageModule {}
