import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { InsectDetailPage } from './insect-detail.page';

describe('InsectDetailPage', () => {
  let component: InsectDetailPage;
  let fixture: ComponentFixture<InsectDetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InsectDetailPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(InsectDetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));


});
