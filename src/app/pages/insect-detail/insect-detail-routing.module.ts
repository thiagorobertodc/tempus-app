import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InsectDetailPage } from './insect-detail.page';

const routes: Routes = [
  {
    path: '',
    component: InsectDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InsectDetailPageRoutingModule {}
