import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ReliefDetailPage } from './relief-detail.page';

describe('ReliefDetailPage', () => {
  let component: ReliefDetailPage;
  let fixture: ComponentFixture<ReliefDetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReliefDetailPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ReliefDetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));


});
