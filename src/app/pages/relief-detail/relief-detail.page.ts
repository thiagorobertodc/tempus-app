import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import { NavController } from '@ionic/angular';
import {ParseService} from "../../services/parse.service";
import {UtilsService} from "../../services/utils.service";
import {EventsService} from "../../services/events.service";
import { TimerService } from 'src/app/services/timer.service';

@Component({
  selector: 'app-relief-detail',
  templateUrl: './relief-detail.page.html',
  styleUrls: ['./relief-detail.page.scss'],
})
export class ReliefDetailPage implements OnInit {
  public relief: any;
  public addedPhoto: any;
  public loading: any;
  public favRelief: any;
  constructor(public route: ActivatedRoute,
    public parse: ParseService,
    public utils: UtilsService,
    public events: EventsService,
    public navCtrl: NavController,
    public timer: TimerService) { }

  async ngOnInit() {
    this.loading = true;
    try {
      await this.utils.showLoading();
      const reliefId = this.route.snapshot.paramMap.get('id');
      this.parse.getItem('RegisteredReliefs', reliefId).then(async result => {
      this.relief = result;
      this.addedPhoto = result.get('photo').url();
      this.loading = false;
      this.utils.dismissLoading();
      await this.timer.stopTimer(6);
      this.timer.startTimer();
    }).catch(error => {
      this.utils.dismissLoading();
      this.loading = false;
      this.utils.showToaster('Erro ao carregar detalhes do relevo. Tente novamente.', 3000);
      this.navCtrl.navigateBack('/tabs/tab1');
    });
    } catch {
      this.utils.dismissLoading();
      this.loading = false;
      this.utils.showToaster('Erro ao carregar detalhes do relevo. Tente novamente.', 3000);
      this.navCtrl.navigateBack('/tabs/tab1');
    }

  }

  favorite(relief: any, favRelief: boolean) {
    const user = this.parse.currentUser;

    if (user) {
      if (!favRelief) {
        this.parse.putRelation(user, 'likedReliefs', relief).then(async () => {
          await this.utils.dismissLoading();
          this.favRelief = true;
          await this.timer.stopTimer(7);
          this.events.publish({updateFavorites: true});
        });
      } else if (favRelief) {
        this.parse.removeRelation(user, 'likedReliefs', relief).then(() => {
          this.utils.dismissLoading();
          this.favRelief = false;
          this.events.publish({updateFavorites: true});
        });
      }
    } else {
      this.navCtrl.navigateRoot('/login');
    }
  }

}
