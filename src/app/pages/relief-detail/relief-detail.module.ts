import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReliefDetailPageRoutingModule } from './relief-detail-routing.module';

import { ReliefDetailPage } from './relief-detail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReliefDetailPageRoutingModule
  ],
  declarations: [ReliefDetailPage]
})
export class ReliefDetailPageModule {}
