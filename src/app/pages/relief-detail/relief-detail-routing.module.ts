import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReliefDetailPage } from './relief-detail.page';

const routes: Routes = [
  {
    path: '',
    component: ReliefDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReliefDetailPageRoutingModule {}
