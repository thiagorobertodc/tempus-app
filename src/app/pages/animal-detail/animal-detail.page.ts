import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {ParseService} from "../../services/parse.service";
import {UtilsService} from "../../services/utils.service";
import {NavController} from "@ionic/angular";
import {DomSanitizer} from "@angular/platform-browser";
import {EventsService} from "../../services/events.service";
import { throwError } from 'rxjs';
import { TimerService } from 'src/app/services/timer.service';

@Component({
  selector: 'app-animal-detail',
  templateUrl: './animal-detail.page.html',
  styleUrls: ['./animal-detail.page.scss'],
})
export class AnimalDetailPage implements OnInit {
public animal: any;
public addedPhoto: any;
public favAnimal = false;
public loading: any;
public objClass: any;
  constructor(public route: ActivatedRoute,
              public parse: ParseService,
              public utils: UtilsService,
              public events: EventsService,
              public router: Router,
              public navCtrl: NavController,
              public sanitizer: DomSanitizer, 
              public timer: TimerService) { }

  async ngOnInit() {
    try {
    await this.utils.showLoading();
    this.loading = true;
    const animalId = this.route.snapshot.paramMap.get('id');
    this.parse.getItem('RegisteredAnimals', animalId).then(async result => {
      console.log(result);
      this.animal = result;
      this.addedPhoto = result.get('photo').url();
      const user = this.parse.currentUser;
      const relation = user.relation('likedAnimals');
      relation.query().find().then(async resp => {
        console.log(resp);
        for (const animal of resp) {
          console.log('procurando');
            if (animal.id === result.id) {
                console.log('iguais');
                this.favAnimal = true;
            }
        }
        this.utils.dismissLoading();
        await this.timer.stopTimer(6);
        this.timer.startTimer();
        this.loading = false;
      });
      this.loading = false;      
    }).catch(error => {
      this.utils.dismissLoading();
      this.utils.showToaster('Erro ao carregar detalhes do animal. Tente novamente.', 3000);
      this.navCtrl.navigateBack('/tabs/tab1');
    });
    }
    catch {
      this.utils.dismissLoading();
      this.utils.showToaster('Erro ao carregar detalhes do animal. Tente novamente.', 3000);
      this.navCtrl.navigateBack('/tabs/tab1');
    }
  }

  favorite(animal: any, favAnimal: boolean) {
    const user = this.parse.currentUser;

    if (user) {
      if (!favAnimal) {
        this.parse.putRelation(user, 'likedAnimals', animal).then(async () => {
          this.utils.dismissLoading();
          this.favAnimal = true;
          await this.timer.stopTimer(7);
          this.events.publish({updateFavorites: true});
        });
      } else if (favAnimal) {
        this.parse.removeRelation(user, 'likedAnimals', animal).then(() => {
          this.utils.dismissLoading();
          this.favAnimal = false;
          this.events.publish({updateFavorites: true});
        });
      }
    } else {
      this.navCtrl.navigateRoot('/login');
    }
  }

}
