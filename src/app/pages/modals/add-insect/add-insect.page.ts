import { Component, OnInit } from '@angular/core';
import {ModalController, NavParams, Platform} from "@ionic/angular";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {ParseService} from "../../../services/parse.service";
import {UtilsService} from "../../../services/utils.service";
import {Geolocation} from "@ionic-native/geolocation/ngx";
import {NativeGeocoder, NativeGeocoderResult} from "@ionic-native/native-geocoder/ngx";
import firebase from "firebase";

import GeoPoint = firebase.firestore.GeoPoint;
import { TimerService } from 'src/app/services/timer.service';
@Component({
  selector: 'app-add-insect',
  templateUrl: './add-insect.page.html',
  styleUrls: ['./add-insect.page.scss'],
})
export class AddInsectPage implements OnInit {
  public form: any;
  public addedPhoto: any;
  public disableButton: any;
  constructor(public modal: ModalController,
              public parse: ParseService,
              public utils: UtilsService,
              public geolocation: Geolocation,
              public platform: Platform,
              public navParams: NavParams,
              public nativeGeocoder: NativeGeocoder,
              public timer: TimerService) { }

  ngOnInit() {
    this.addedPhoto = this.navParams.get('photo');
    this.form = new FormGroup({
      wings: new FormControl('', Validators.compose([
        Validators.maxLength(300)
      ])),
      exoskeleton: new FormControl('', Validators.compose([
        Validators.maxLength(300)
      ])),
      parts: new FormControl('', Validators.compose([
        Validators.maxLength(300) ])),
      name: new FormControl('', Validators.compose([
          Validators.maxLength(300),
          Validators.required
      ]))
    });
  }

  async addInsect() {
    this.disableButton = true;
    await this.utils.showLoading();
    let coords = await this.geolocation.getCurrentPosition({enableHighAccuracy: true});
    let lat = coords.coords.latitude;
    let lng = coords.coords.longitude;
    console.log(lat);
    console.log(lng);
    if (this.platform.is('cordova')) {
      this.nativeGeocoder.reverseGeocode(lat, lng).then(async (res: NativeGeocoderResult[]) => {
        let description = this.platform.is('ios') ? res[0].locality : res[0].subAdministrativeArea;

        const insectObject = {
          user: this.parse.currentUser,
          wings: this.form.get('wings').value,
          exoskeleton: this.form.get('exoskeleton').value,
          parts: this.form.get('parts').value,
          name: this.form.get('name').value,
          coords: new firebase.firestore.GeoPoint(lat, lng) as GeoPoint,
          location: description,
          photo: await this.parse.saveFile('photo.jpg', this.addedPhoto),
          type: 16
        };
        this.parse.saveItem('RegisteredInsects', insectObject).then(() => {
          this.modal.dismiss();
          this.utils.dismissLoading();
        }).catch(error => {
          this.utils.showToaster(error.message, 3000);
        }).finally(
            () => {
              this.disableButton = false;
            }
        );
      });
    } else {
      const insectObject = {
        user: this.parse.currentUser,
        wings: this.form.get('wings').value,
        exoskeleton: this.form.get('exoskeleton').value,
        parts: this.form.get('parts').value,
        name: this.form.get('name').value,
        coords: new firebase.firestore.GeoPoint(lat, lng) as GeoPoint,
        location: 'Ouro Preto - MG'
      };
      this.parse.saveItem('RegisteredInsects', insectObject).then(async () => {
        this.modal.dismiss();
        this.utils.dismissLoading();
        await this.timer.stopTimer(3);
      }).catch(error => {
        this.utils.showToaster(error.message, 3000);
      }).finally(
          () => {
            this.disableButton = false;
          }
      );
    }
  }

}
