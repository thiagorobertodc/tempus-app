import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import {RouterTestingModule} from "@angular/router/testing";
import {ModalController, NavParams} from "@ionic/angular";
import {Geolocation} from "@ionic-native/geolocation/ngx";
import { AddInsectPage } from './add-insect.page';
import {ParseService} from "../../../services/parse.service";
import { TimerService } from 'src/app/services/timer.service';
import {NativeGeocoder, NativeGeocoderResult} from "@ionic-native/native-geocoder/ngx";
import { MockParse, ParseMock } from 'src/mocks';
import { DebugElement } from '@angular/core';
import * as faker from 'faker';
import { By } from '@angular/platform-browser';

describe('AddInsectPage', () => {
  let parseSpy, modalSpy, cameraSpy, geolocationSpy, timerSpy, nativeGeocoderSpy, modalCtrlSpy, getListSpy, navParamsSpy;
  let component: AddInsectPage;
  let fixture: ComponentFixture<AddInsectPage>;
  let de: DebugElement;

  beforeEach(async(() => {
    modalSpy = jasmine.createSpyObj('Modal', ['present']);
    modalCtrlSpy = jasmine.createSpyObj('ModalController', ['create']);
    modalCtrlSpy.create.and.callFake(function () {
        return modalSpy;
    });
    geolocationSpy = jasmine.createSpyObj('Geolocation', ['getCurrentPosition']);
    nativeGeocoderSpy = jasmine.createSpyObj('NativeGeocoder', ['reverseGeocode']);
    navParamsSpy = jasmine.createSpyObj('NavParams', ['get']);
    cameraSpy = jasmine.createSpyObj('CameraService', ['getMediaDefault']);
    getListSpy = jasmine.createSpy().and.returnValue(Promise.resolve());
    parseSpy = new MockParse();
    parseSpy.getList = getListSpy;
    timerSpy = jasmine.createSpyObj('TimerService', ['startTimer']);
    TestBed.configureTestingModule({
      declarations: [ AddInsectPage ],
      imports: [IonicModule.forRoot(), RouterTestingModule],
      providers: [
        {provide: ModalController, useValue: modalCtrlSpy},
        {provide: AddInsectPage, useClass: ParseMock},
        {provide: Geolocation, useValue: geolocationSpy},
        {provide: ParseService, useValue: parseSpy},
        {provide: TimerService, useValue: timerSpy},
        {provide: NavParams, useValue: navParamsSpy},
        {provide: NativeGeocoder, useValue: nativeGeocoderSpy},
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(AddInsectPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have a form input', () => {
    expect(component.form).toBeTruthy();
  });

  it('should correctly render the passed name Input value', () => {
    const name = faker.name.findName();
    fixture.debugElement.query(By.css('#name')).nativeElement.value = name;
    fixture.debugElement.query(By.css('#name')).nativeElement.dispatchEvent(new Event('input'));
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(component.form.get('name').value).toBe(name); // this pass
    });
  });

  it('should correctly render the passed wings Input value', () => {
    const wings = faker.name.findName();
    fixture.debugElement.query(By.css('#wings')).nativeElement.value = wings;
    fixture.debugElement.query(By.css('#wings')).nativeElement.dispatchEvent(new Event('input'));
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(component.form.get('wings').value).toBe(wings); // this pass
    });
  });

  it('should correctly render the passed exoskeleton Input value', () => {
    const exoskeleton = faker.name.findName();
    fixture.debugElement.query(By.css('#exoskeleton')).nativeElement.value = exoskeleton;
    fixture.debugElement.query(By.css('#exoskeleton')).nativeElement.dispatchEvent(new Event('input'));
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(component.form.get('exoskeleton').value).toBe(exoskeleton); // this pass
    });
  });

  it("should call parse SaveItem on button click", () => {
    let addInsect = fixture.debugElement.injector.get(AddInsectPage);
    spyOn(addInsect, "addInsect");
    const name = faker.name.findName();
    fixture.debugElement.query(By.css('#name')).nativeElement.value = name;
    fixture.debugElement.query(By.css('#name')).nativeElement.dispatchEvent(new Event('input'));

    
    const wings = faker.name.findName();
    fixture.debugElement.query(By.css('#wings')).nativeElement.value = wings;
    fixture.debugElement.query(By.css('#wings')).nativeElement.dispatchEvent(new Event('input'));
    
    const exoskeleton = faker.name.findName();
    fixture.debugElement.query(By.css('#exoskeleton')).nativeElement.value = exoskeleton;
    fixture.debugElement.query(By.css('#exoskeleton')).nativeElement.dispatchEvent(new Event('input'));
    
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      de = fixture.debugElement.query(By.css("#add-insect"));
      de.triggerEventHandler("click", null);
      expect(addInsect.addInsect).toHaveBeenCalled();
    });
  });
});
