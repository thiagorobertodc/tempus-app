import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddTreePageRoutingModule } from './add-tree-routing.module';

import { AddTreePage } from './add-tree.page';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        AddTreePageRoutingModule,
        ReactiveFormsModule
    ],
  declarations: [AddTreePage]
})
export class AddTreePageModule {}
