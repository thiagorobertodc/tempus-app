import { Component, OnInit } from '@angular/core';
import {ModalController, NavParams, Platform} from "@ionic/angular";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {ParseService} from "../../../services/parse.service";
import {UtilsService} from "../../../services/utils.service";
import {Geolocation} from "@ionic-native/geolocation/ngx";
import {NativeGeocoder, NativeGeocoderResult} from "@ionic-native/native-geocoder/ngx";
import firebase from "firebase";
import GeoPoint = firebase.firestore.GeoPoint;
import { TimerService } from 'src/app/services/timer.service';
@Component({
  selector: 'app-add-tree',
  templateUrl: './add-tree.page.html',
  styleUrls: ['./add-tree.page.scss'],
})
export class AddTreePage implements OnInit {
  public form: any;
  public addedPhoto: any;
  public disableButton: any;
  constructor(public modal: ModalController,
              public parse: ParseService,
              public utils: UtilsService,
              public geolocation: Geolocation,
              public platform: Platform,
              public navParams: NavParams,
              public nativeGeocoder: NativeGeocoder,
              public timer: TimerService) { }

  ngOnInit() {
    this.addedPhoto = this.navParams.get('photo');
    this.form = new FormGroup({
      topShape: new FormControl('', Validators.compose([
        Validators.maxLength(300)
      ])),
      leafShape: new FormControl('', Validators.compose([
        Validators.maxLength(300)
      ])),
      fruitful: new FormControl(false),
      name: new FormControl('', Validators.compose([
        Validators.required,
        Validators.maxLength(300)
      ]))
    });
  }
  async addTree() {
    this.disableButton = true;
    await this.utils.showLoading();
    let coords = await this.geolocation.getCurrentPosition({enableHighAccuracy: true});
    let lat = coords.coords.latitude;
    let lng = coords.coords.longitude;
    console.log(lat);
    console.log(lng);
    if (this.platform.is('cordova')) {
      this.nativeGeocoder.reverseGeocode(lat, lng).then(async (res: NativeGeocoderResult[]) => {
        let description = this.platform.is('ios') ? res[0].locality : res[0].subAdministrativeArea;

        const treeObject = {
          user: this.parse.currentUser,
          topShape: this.form.get('topShape').value,
          leafShape: this.form.get('leafShape').value,
          fruitful: this.form.get('fruitful').value,
          name: this.form.get('name').value,
          coords: new firebase.firestore.GeoPoint(lat, lng) as GeoPoint,
          location: description,
          photo: await this.parse.saveFile('photo.jpg', this.addedPhoto),
          type: 14
        };
        this.parse.saveItem('RegisteredTrees', treeObject).then(() => {
          this.modal.dismiss();
          this.utils.dismissLoading();
        }).catch(error => {
          this.utils.showToaster(error.message, 3000);
        }).finally(
            () => {
              this.disableButton = false;
            }
        );
      });
    } else {
      const treeObject = {
        user: this.parse.currentUser,
        topShape: this.form.get('topShape').value,
        leafShape: this.form.get('leafShape').value,
        fruitful: this.form.get('fruitful').value,
        name: this.form.get('name').value,
        coords: new firebase.firestore.GeoPoint(lat, lng) as GeoPoint,
        location: 'Ouro Preto - MG'
      };
      this.parse.saveItem('RegisteredTrees', treeObject).then(async () => {
        this.modal.dismiss();
        await this.timer.stopTimer(3);
      }).catch(error => {
        this.utils.showToaster(error.message, 3000);
      }).finally(
          () => {
            this.disableButton = false;
          }
      );
    }

  }
}
