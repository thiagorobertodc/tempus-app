import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddTreePage } from './add-tree.page';

const routes: Routes = [
  {
    path: '',
    component: AddTreePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddTreePageRoutingModule {}
