import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule, ModalController, NavParams } from '@ionic/angular';
import {RouterTestingModule} from "@angular/router/testing";
import { FormControl, FormGroup, Validators } from '@angular/forms';
import {Geolocation} from "@ionic-native/geolocation/ngx";
import { ParseService } from 'src/app/services/parse.service';
import { TimerService } from 'src/app/services/timer.service';
import { NativeGeocoder } from '@ionic-native/native-geocoder/ngx';
import { MockParse } from 'src/mocks';
import { AddTreePage } from './add-tree.page';

class MockNavParams {
  get: jasmine.Spy<any>;
}

class MockCustomClass {
  get: jasmine.Spy<any>;
}
describe('AddTreePage', () => {
  let component: AddTreePage;
  let fixture: ComponentFixture<AddTreePage>;
  let parseSpy, modalSpy, cameraSpy, geolocationSpy, timerSpy, nativeGeocoderSpy, modalCtrlSpy, getItemSpy, navParamsSpy, nvpGetSpy, customClassSpy;
  let ccGetSpy;

  beforeEach(async(() => {
    customClassSpy = new MockCustomClass();
    customClassSpy.get = ccGetSpy;
    modalSpy = jasmine.createSpyObj('Modal', ['present']);
    modalCtrlSpy = jasmine.createSpyObj('ModalController', ['create']);
    modalCtrlSpy.create.and.callFake(function () {
        return modalSpy;
    });
    nvpGetSpy = jasmine.createSpy().and.returnValue({
      'photo': 'a_photo_string',
    });
    
    navParamsSpy = new MockNavParams();
    navParamsSpy.get = nvpGetSpy;
    geolocationSpy = jasmine.createSpyObj('Geolocation', ['getCurrentPosition']);
    nativeGeocoderSpy = jasmine.createSpyObj('NativeGeocoder', ['reverseGeocode']);
    cameraSpy = jasmine.createSpyObj('CameraService', ['getMediaDefault']);
    getItemSpy = jasmine.createSpy().and.returnValue(Promise.resolve());
    parseSpy = new MockParse();
    parseSpy.getItem = getItemSpy;
    timerSpy = jasmine.createSpyObj('TimerService', ['startTimer']);
    navParamsSpy = new MockNavParams();
    navParamsSpy.get = nvpGetSpy;
    TestBed.configureTestingModule({
      declarations: [ AddTreePage ],
      imports: [IonicModule.forRoot(), RouterTestingModule],
      providers: [
        {provide: NavParams, useValue: navParamsSpy},
        {provide: ModalController, useValue: modalCtrlSpy},
        {provide: Geolocation, useValue: geolocationSpy},
        {provide: ParseService, useValue: parseSpy},
        {provide: TimerService, useValue: timerSpy},
        {provide: NativeGeocoder, useValue: nativeGeocoderSpy},
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(AddTreePage);
    component = fixture.componentInstance;
    component.form = new FormGroup({
      topShape: new FormControl('', Validators.compose([
        Validators.maxLength(300)
      ])),
      leafShape: new FormControl('', Validators.compose([
        Validators.maxLength(300)
      ])),
      fruitful: new FormControl(false),
      name: new FormControl('', Validators.compose([
        Validators.required,
        Validators.maxLength(300)
      ]))
    });
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
