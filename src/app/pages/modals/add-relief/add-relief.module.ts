import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddReliefPageRoutingModule } from './add-relief-routing.module';

import { AddReliefPage } from './add-relief.page';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        AddReliefPageRoutingModule,
        ReactiveFormsModule
    ],
  declarations: [AddReliefPage]
})
export class AddReliefPageModule {}
