import { Component, OnInit } from '@angular/core';
import {ModalController, NavParams, Platform} from "@ionic/angular";
import {ParseService} from "../../../services/parse.service";
import {UtilsService} from "../../../services/utils.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Geolocation} from "@ionic-native/geolocation/ngx";
import {NativeGeocoder, NativeGeocoderResult} from "@ionic-native/native-geocoder/ngx";
import firebase from "firebase";
import GeoPoint = firebase.firestore.GeoPoint;
import { TimerService } from 'src/app/services/timer.service';
@Component({
  selector: 'app-add-relief',
  templateUrl: './add-relief.page.html',
  styleUrls: ['./add-relief.page.scss'],
})
export class AddReliefPage implements OnInit {
  public form: any;
  public classes: any;
  public reliefs: any;
  public reliefClass: any;
  public chosenRelief: any;
  public disableButton: any;
  public addedPhoto: any;
  constructor(public modal: ModalController,
              public parse: ParseService,
              public utils: UtilsService,
              public geolocation: Geolocation,
              public platform: Platform,
              public navParams: NavParams,
              public timer: TimerService,
              public nativeGeocoder: NativeGeocoder) { }

  ngOnInit() {
    this.addedPhoto = this.navParams.get('photo');
    this.form = new FormGroup({
      name: new FormControl('', Validators.compose([
        Validators.maxLength(300),
        Validators.required
      ])),
      description: new FormControl('', Validators.maxLength(500)),
    });
    this.parse.getList('ReliefClasses', null, null, null, null, null, '*').then(r => {
      this.classes = r;
      // console.log(this.genres[0]);
      // console.log(r);
    });
  }

  getReliefs() {
    console.log(this.reliefClass);
    this.parse.getList('Relief', [['myClass', '=', this.reliefClass]], null, null, null, null, '*').then(r => {
      this.reliefs = r;
      // console.log(this.genres[0]);
      console.log(r);
    });
  }

  async addRelief() {
    this.disableButton = true;
    await this.utils.showLoading();
    let coords = await this.geolocation.getCurrentPosition({enableHighAccuracy: true});
    let lat = coords.coords.latitude;
    let lng = coords.coords.longitude;
    console.log(lat);
    console.log(lng);
    if (this.platform.is('cordova')) {
      this.nativeGeocoder.reverseGeocode(lat, lng).then(async (res: NativeGeocoderResult[]) => {
        let description = this.platform.is('ios') ? res[0].locality : res[0].subAdministrativeArea;

        const reliefObject = {
          user: this.parse.currentUser,
          reliefType: this.chosenRelief,
          name: this.form.get('name').value,
          coords: new firebase.firestore.GeoPoint(lat, lng) as GeoPoint,
          location: description,
          photo: await this.parse.saveFile('photo.jpg', this.addedPhoto),
          description: this.form.get('description').value,
          type: 13
        };
        this.parse.saveItem('RegisteredReliefs', reliefObject).then(() => {
          this.modal.dismiss();
          this.utils.dismissLoading();
        }).catch(error => {
          this.utils.showToaster(error.message, 3000);
        }).finally(
            () => {
              this.disableButton = false;
            }
        );
      });
    } else {
      const reliefObject = {
        user: this.parse.currentUser,
        reliefType: this.chosenRelief,
        name: this.form.get('name').value,
        coords: new firebase.firestore.GeoPoint(lat, lng) as GeoPoint,
        location: 'Ouro Preto - MG'
      };
      this.parse.saveItem('RegisteredReliefs', reliefObject).then(async () => {
        this.modal.dismiss();
        await this.timer.stopTimer(3);
        this.utils.dismissLoading();
      }).catch(error => {
        this.utils.showToaster(error.message, 3000);
      }).finally(
          () => {
            this.disableButton = false;
          }
      );
    }

  }

}
