import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule, ModalController, NavParams } from '@ionic/angular';
import {RouterTestingModule} from "@angular/router/testing";
import { AddReliefPage } from './add-relief.page';
import {Geolocation} from "@ionic-native/geolocation/ngx";
import { MockParse } from 'src/mocks';
import { ParseService } from 'src/app/services/parse.service';
import { TimerService } from 'src/app/services/timer.service';
import { NativeGeocoder } from '@ionic-native/native-geocoder/ngx';

class MockNavParams {
  get: jasmine.Spy<any>;
}

class MockCustomClass {
  get: jasmine.Spy<any>;
}
describe('AddReliefPage', () => {
  let component: AddReliefPage;
  let fixture: ComponentFixture<AddReliefPage>;
  let parseSpy, modalSpy, cameraSpy, geolocationSpy, timerSpy, nativeGeocoderSpy, modalCtrlSpy, getListSpy, navParamsSpy, nvpGetSpy, customClassSpy;
  let ccGetSpy;
  beforeEach(async(() => {
    customClassSpy = new MockCustomClass();
    customClassSpy.get = ccGetSpy;
    modalSpy = jasmine.createSpyObj('Modal', ['present']);
    modalCtrlSpy = jasmine.createSpyObj('ModalController', ['create']);
    modalCtrlSpy.create.and.callFake(function () {
        return modalSpy;
    });
    nvpGetSpy = jasmine.createSpy().and.returnValue({
      'photo': 'a_photo_string',
    });
    
    navParamsSpy = new MockNavParams();
    navParamsSpy.get = nvpGetSpy;
    geolocationSpy = jasmine.createSpyObj('Geolocation', ['getCurrentPosition']);
    nativeGeocoderSpy = jasmine.createSpyObj('NativeGeocoder', ['reverseGeocode']);
    cameraSpy = jasmine.createSpyObj('CameraService', ['getMediaDefault']);
    getListSpy = jasmine.createSpy().and.returnValue(Promise.resolve());
    parseSpy = new MockParse();
    parseSpy.getList = getListSpy;
    timerSpy = jasmine.createSpyObj('TimerService', ['startTimer']);
    navParamsSpy = new MockNavParams();
    navParamsSpy.get = nvpGetSpy;
    TestBed.configureTestingModule({
      declarations: [ AddReliefPage ],
      imports: [IonicModule.forRoot(), RouterTestingModule],
      providers: [
        {provide: NavParams, useValue: navParamsSpy},
        {provide: ModalController, useValue: modalCtrlSpy},
        {provide: Geolocation, useValue: geolocationSpy},
        {provide: ParseService, useValue: parseSpy},
        {provide: TimerService, useValue: timerSpy},
        {provide: NativeGeocoder, useValue: nativeGeocoderSpy},
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(AddReliefPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
