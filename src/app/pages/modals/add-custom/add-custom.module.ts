import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddCustomPageRoutingModule } from './add-custom-routing.module';

import { AddCustomPage } from './add-custom.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddCustomPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [AddCustomPage]
})
export class AddCustomPageModule {}
