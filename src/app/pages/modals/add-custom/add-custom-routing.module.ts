import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddCustomPage } from './add-custom.page';

const routes: Routes = [
  {
    path: '',
    component: AddCustomPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddCustomPageRoutingModule {}
