import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import { ParseService } from 'src/app/services/parse.service';
import { TimerService } from 'src/app/services/timer.service';
import { UtilsService } from 'src/app/services/utils.service';

@Component({
  selector: 'app-add-custom',
  templateUrl: './add-custom.page.html',
  styleUrls: ['./add-custom.page.scss'],
})
export class AddCustomPage implements OnInit {
  public form: any;
  public index: any;
  public isLoading: any;
  constructor(public modalCtrl: ModalController,
              public parse: ParseService,
              public utils: UtilsService,
              public timer: TimerService,) { }

  ngOnInit() {
    this.isLoading = false;
    this.form = new FormGroup({
      name: new FormControl('', Validators.compose([
        Validators.maxLength(300),
          Validators.required
      ]))
    });
  }

  toggleIcon(chosenIndex) {
    console.log("called");
    this.index = chosenIndex;
  }

  async addCustomCategory() {
  this.isLoading = true;
  await this.utils.showLoading();
    if (this.index == null) {
      this.utils.showToaster('Escolha um ícone.', 3000);
      this.isLoading = false;
      this.utils.dismissLoading();
      return;
    }

    if (!this.form.valid) {
      this.utils.showToaster('Dê um nome para categoria.', 3000);
      this.utils.dismissLoading();
      this.isLoading = false;
      return;
    }

    const customClass = {
      name: this.form.get('name').value,
      type: this.index,
      user: this.parse.currentUser,
    }
    this.parse.saveItem('CustomClass', customClass).then(async () => {
      this.utils.dismissLoading();
      await this.timer.stopTimer(4);
      this.isLoading = false;
      this.modalCtrl.dismiss();
    }).catch(error => {
      this.utils.showToaster(error.message, 3000);
  }).finally(() => {
    this.isLoading = false;
  });
}

}
