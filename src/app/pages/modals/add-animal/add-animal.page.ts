import { Component, OnInit } from '@angular/core';
import {ParseService} from "../../../services/parse.service";
import {ModalController, NavParams, Platform} from "@ionic/angular";
import {UtilsService} from "../../../services/utils.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";
import {Geolocation} from "@ionic-native/geolocation/ngx";
import GeoPoint = firebase.firestore.GeoPoint;
import {NativeGeocoder, NativeGeocoderResult} from "@ionic-native/native-geocoder/ngx";
import { TimerService } from 'src/app/services/timer.service';


@Component({
  selector: 'app-add-animal',
  templateUrl: './add-animal.page.html',
  styleUrls: ['./add-animal.page.scss'],
})
export class AddAnimalPage implements OnInit {
  public form: any;
  public addedPhoto: any;
  public classes: any;
  public animals: any;
  public animalClass: any;
  public chosenAnimal: any;
  public disableButton: any;
  constructor(public parse: ParseService,
              public modal: ModalController,
              public utils: UtilsService,
              public geolocation: Geolocation,
              public platform: Platform,
              public navParams: NavParams,
              public timer: TimerService,
              public nativeGeocoder: NativeGeocoder) {
  }

  ngOnInit() {
    this.disableButton = true;
    this.addedPhoto = this.navParams.get('photo');
    console.log('A photo é: ' + this.addedPhoto);
    this.form = new FormGroup({
      name: new FormControl('', Validators.compose([
        Validators.maxLength(300),
          Validators.required
      ])),
      description: new FormControl('', Validators.maxLength(500)),
    });
    this.parse.getList('AnimalClasses', null, null, null, null, null, '*').then(r => {
      this.classes = r;
      // console.log(this.genres[0]);
      // console.log(r);
    });
  }

  getAnimals() {
    this.parse.getList('Animals', [['myClass', '=', this.animalClass]], null, null, null, null, '*').then(r => {
      this.animals = r;
      // console.log(this.genres[0]);
      console.log(r);
    });
  }

  async addAnimal() {
    this.disableButton = true;
    await this.utils.showLoading();
    let coords = await this.geolocation.getCurrentPosition({enableHighAccuracy: true});
    let lat = coords.coords.latitude;
    let lng = coords.coords.longitude;
    console.log(lat);
    console.log(lng);
    if (this.platform.is('cordova')) {
      this.nativeGeocoder.reverseGeocode(lat, lng).then(async (res: NativeGeocoderResult[]) => {
        let description = this.platform.is('ios') ? res[0].locality : res[0].subAdministrativeArea;
        const animalObject = {
          user: this.parse.currentUser,
          animalType: this.chosenAnimal,
          name: this.form.get('name').value,
          coords: new firebase.firestore.GeoPoint(lat, lng) as GeoPoint,
          location: description,
          description: this.form.get('description').value,
          photo: await this.parse.saveFile('photo.jpg', this.addedPhoto),
          type: 12
        };
        this.parse.saveItem('RegisteredAnimals', animalObject).then(() => {
          this.utils.dismissLoading();
          this.modal.dismiss();
        }).catch(error => {
          this.utils.showToaster(error.message, 3000);
        }).finally(
            () => {
              this.disableButton = false;
            }
        );
      });
    } else {
      const animalObject = {
        user: this.parse.currentUser,
        animalType: this.chosenAnimal,
        name: this.form.get('name').value,
        coords: new firebase.firestore.GeoPoint(lat, lng) as GeoPoint,
        location: 'Ouro Preto - MG'
      };
      this.parse.saveItem('RegisteredAnimals', animalObject).then(async () => {
        this.utils.dismissLoading();
        await this.timer.stopTimer(2);
        this.modal.dismiss();
      }).catch(error => {
        this.utils.showToaster(error.message, 3000);
      }).finally(
          () => {
            this.disableButton = false;
          }
      );
    }

  }

}
