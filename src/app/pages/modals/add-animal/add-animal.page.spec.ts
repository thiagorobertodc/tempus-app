import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import {RouterTestingModule} from "@angular/router/testing";
import {ModalController, NavParams} from "@ionic/angular";
import {Geolocation} from "@ionic-native/geolocation/ngx";
import { AddAnimalPage } from './add-animal.page';
import {ParseService} from "../../../services/parse.service";
import { TimerService } from 'src/app/services/timer.service';
import {NativeGeocoder, NativeGeocoderResult} from "@ionic-native/native-geocoder/ngx";
import { MockParse, ParseMock } from 'src/mocks';
import { DebugElement } from '@angular/core';
import * as faker from 'faker';
import { By } from '@angular/platform-browser';

describe('AddAnimalPage', () => {
  let parseSpy, modalSpy, cameraSpy, geolocationSpy, timerSpy, nativeGeocoderSpy, modalCtrlSpy, getListSpy, navParamsSpy;
  let component: AddAnimalPage;
  let fixture: ComponentFixture<AddAnimalPage>;
  let de: DebugElement;

  beforeEach(async(() => {
    modalSpy = jasmine.createSpyObj('Modal', ['present']);
    modalCtrlSpy = jasmine.createSpyObj('ModalController', ['create']);
    modalCtrlSpy.create.and.callFake(function () {
        return modalSpy;
    });
    geolocationSpy = jasmine.createSpyObj('Geolocation', ['getCurrentPosition']);
    nativeGeocoderSpy = jasmine.createSpyObj('NativeGeocoder', ['reverseGeocode']);
    navParamsSpy = jasmine.createSpyObj('NavParams', ['get']);
    cameraSpy = jasmine.createSpyObj('CameraService', ['getMediaDefault']);
    getListSpy = jasmine.createSpy().and.returnValue(Promise.resolve());
    parseSpy = new MockParse();
    parseSpy.getList = getListSpy;
    timerSpy = jasmine.createSpyObj('TimerService', ['startTimer']);
    TestBed.configureTestingModule({
      declarations: [ AddAnimalPage ],
      imports: [IonicModule.forRoot(), RouterTestingModule],
      providers: [
        {provide: ModalController, useValue: modalCtrlSpy},
        {provide: AddAnimalPage, useClass: ParseMock},
        {provide: Geolocation, useValue: geolocationSpy},
        {provide: ParseService, useValue: parseSpy},
        {provide: TimerService, useValue: timerSpy},
        {provide: NavParams, useValue: navParamsSpy},
        {provide: NativeGeocoder, useValue: nativeGeocoderSpy},
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(AddAnimalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have a form input', () => {
    expect(component.form).toBeTruthy();
  });

  it('should correctly render the passed name Input value', () => {
    const name = faker.name.findName();
    fixture.debugElement.query(By.css('#name')).nativeElement.value = name;
    fixture.debugElement.query(By.css('#name')).nativeElement.dispatchEvent(new Event('input'));
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(component.form.get('name').value).toBe(name); // this pass
    });
  });

  it('should correctly render the passed description Input value', () => {
    const description = faker.name.findName();
    fixture.debugElement.query(By.css('#description')).nativeElement.value = description;
    fixture.debugElement.query(By.css('#description')).nativeElement.dispatchEvent(new Event('input'));
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(component.form.get('description').value).toBe(description); // this pass
    });
  });

  it("should call parse SaveItem on button click", () => {
    let addAnimal = fixture.debugElement.injector.get(AddAnimalPage);
    spyOn(addAnimal, "addAnimal");
    const name = faker.name.findName();
    fixture.debugElement.query(By.css('#name')).nativeElement.value = name;
    fixture.debugElement.query(By.css('#name')).nativeElement.dispatchEvent(new Event('input'));

    
    const description = faker.name.findName();
    fixture.debugElement.query(By.css('#description')).nativeElement.value = description;
    fixture.debugElement.query(By.css('#description')).nativeElement.dispatchEvent(new Event('input'));
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      de = fixture.debugElement.query(By.css("#add-animal"));
      de.triggerEventHandler("click", null);
      expect(addAnimal.addAnimal).toHaveBeenCalled();
    });
  });
});
