import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddCustomObjectPageRoutingModule } from './add-custom-object-routing.module';

import { AddCustomObjectPage } from './add-custom-object.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddCustomObjectPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [AddCustomObjectPage]
})
export class AddCustomObjectPageModule {}
