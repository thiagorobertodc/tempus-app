import { async, ComponentFixture, fakeAsync, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import {RouterTestingModule} from "@angular/router/testing";
import {ModalController, NavParams} from "@ionic/angular";
import {Geolocation} from "@ionic-native/geolocation/ngx";
import {ParseService} from "../../../services/parse.service";
import { TimerService } from 'src/app/services/timer.service';
import {NativeGeocoder, NativeGeocoderResult} from "@ionic-native/native-geocoder/ngx";
import { AddCustomObjectPage } from './add-custom-object.page';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import * as faker from 'faker';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
class MockParse {
  getItem: jasmine.Spy<any>;  
}

class MockNavParams {
  get: jasmine.Spy<any>;
}

var MockCustomClass = function () {
  var get = function () {
      return {
        'name': 'Custom_Name'
      };
  };

  return {
      get: get
  };
};

describe('AddCustomObjectPage', () => {
  let parseSpy, modalSpy, cameraSpy, geolocationSpy, timerSpy, nativeGeocoderSpy, modalCtrlSpy, getItemSpy, navParamsSpy, nvpGetSpy, customClassSpy;
  let ccGetSpy;
  let component: AddCustomObjectPage;
  let fixture: ComponentFixture<AddCustomObjectPage>;
  let de: DebugElement;

  beforeEach(async(() => {
    ccGetSpy = jasmine.createSpy().and.returnValue({
      'name': 'Custom_Name'
    });
    var someObject = jasmine.createSpyObj('customClass', ['get']);
    someObject.get.and.callFake(function() {
    return {
      'name': 'Custom_Name'
    };
});
    modalSpy = jasmine.createSpyObj('Modal', ['present']);
    modalCtrlSpy = jasmine.createSpyObj('ModalController', ['create']);
    modalCtrlSpy.create.and.callFake(function () {
        return modalSpy;
    });
    nvpGetSpy = jasmine.createSpy().and.returnValue({
      'id': 'any_id',
      'photo': 'a_photo_string',
      'classId': 'any_id',
    });
    
    navParamsSpy = new MockNavParams();
    navParamsSpy.get = nvpGetSpy;
    geolocationSpy = jasmine.createSpyObj('Geolocation', ['getCurrentPosition']);
    nativeGeocoderSpy = jasmine.createSpyObj('NativeGeocoder', ['reverseGeocode']);
    cameraSpy = jasmine.createSpyObj('CameraService', ['getMediaDefault']);
    getItemSpy = jasmine.createSpy().and.returnValue(Promise.resolve());
    parseSpy = new MockParse();
    parseSpy.getItem = getItemSpy;
    timerSpy = jasmine.createSpyObj('TimerService', ['startTimer']);
    TestBed.configureTestingModule({
      declarations: [ AddCustomObjectPage ],
      imports: [IonicModule.forRoot(), RouterTestingModule],
      providers: [
        {provide: ModalController, useValue: modalCtrlSpy},
  
        {provide: Geolocation, useValue: geolocationSpy},
        {provide: ParseService, useValue: parseSpy},
        {provide: TimerService, useValue: timerSpy},
        {provide: NavParams, useValue: navParamsSpy},
        {provide: NativeGeocoder, useValue: nativeGeocoderSpy},
      ]
    }).compileComponents();
    
    fixture = TestBed.createComponent(AddCustomObjectPage);
    component = fixture.componentInstance;
    component.form = new FormGroup({
      name: new FormControl('', Validators.compose([
        Validators.maxLength(300),
          Validators.required
      ])),
      description: new FormControl('', Validators.maxLength(500)),
    });
    component.customClass = someObject;
    fixture.detectChanges();
  }));

  it('should create', () => {
    fakeAsync(() => {
        expect(component).toBeTruthy();
    });
});

it('should correctly render the passed name Input value', () => {
  const name = faker.name.findName();
  fixture.debugElement.query(By.css('#name')).nativeElement.value = name;
  fixture.debugElement.query(By.css('#name')).nativeElement.dispatchEvent(new Event('input'));
  fixture.detectChanges();
  fixture.whenStable().then(() => {
    expect(component.form.get('name').value).toBe(name); // this pass
  });
});

it('should correctly render the passed description Input value', () => {
  const description = faker.name.findName();
  fixture.debugElement.query(By.css('#description')).nativeElement.value = description;
  fixture.debugElement.query(By.css('#description')).nativeElement.dispatchEvent(new Event('input'));
  fixture.detectChanges();
  fixture.whenStable().then(() => {
    expect(component.form.get('description').value).toBe(description); // this pass
  });
});

it("should call parse SaveItem on button click", () => {
  let addCustom = fixture.debugElement.injector.get(AddCustomObjectPage);
  spyOn(addCustom, "add");
  const name = faker.name.findName();
  fixture.debugElement.query(By.css('#name')).nativeElement.value = name;
  fixture.debugElement.query(By.css('#name')).nativeElement.dispatchEvent(new Event('input'));

  
  const description = faker.name.findName();
  fixture.debugElement.query(By.css('#description')).nativeElement.value = description;
  fixture.debugElement.query(By.css('#description')).nativeElement.dispatchEvent(new Event('input'));
  fixture.detectChanges();
  fixture.whenStable().then(() => {
    de = fixture.debugElement.query(By.css("#add-custom"));
    de.triggerEventHandler("click", null);
    expect(addCustom.add).toHaveBeenCalled();
  });
});
});
