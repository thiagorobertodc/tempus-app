import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ModalController, NavParams, Platform} from '@ionic/angular';
import {Geolocation} from "@ionic-native/geolocation/ngx";
import { ParseService } from 'src/app/services/parse.service';
import { UtilsService } from 'src/app/services/utils.service';
import {NativeGeocoder, NativeGeocoderResult} from "@ionic-native/native-geocoder/ngx";
import firebase from "firebase";
import GeoPoint = firebase.firestore.GeoPoint;
import { TimerService } from 'src/app/services/timer.service';
@Component({
  selector: 'app-add-custom-object',
  templateUrl: './add-custom-object.page.html',
  styleUrls: ['./add-custom-object.page.scss'],
})
export class AddCustomObjectPage implements OnInit {
  public custom;
  public customClass: any;
  public form: any;
  public isLoading: any;
  public addedPhoto: any;
  constructor(public navParams: NavParams,
              public parse: ParseService,
              public utils: UtilsService,
              public geolocation: Geolocation,
              public platform: Platform,
              public modalCtrl: ModalController,
              public nativeGeocoder: NativeGeocoder,
              public timer: TimerService) { }

  async ngOnInit() {
    this.addedPhoto = this.navParams.get('photo');
    let id = this.navParams.get('id');
    this.customClass = await this.parse.getItem('CustomClass', id);
    this.form = new FormGroup({
      name: new FormControl('', Validators.compose([
        Validators.maxLength(300),
          Validators.required
      ])),
      description: new FormControl('', Validators.maxLength(500)),
    });
    this.customClass = await this.parse.getItem('CustomClass', this.navParams.get('classId')); 
  }

  async add() {
    await this.utils.showLoading();
    this.isLoading = true;
    let coords = await this.geolocation.getCurrentPosition({enableHighAccuracy: true});
    let lat = coords.coords.latitude;
    let lng = coords.coords.longitude;
    console.log(lat);
    console.log(lng);
    if (this.platform.is('cordova')) {
      this.nativeGeocoder.reverseGeocode(lat, lng).then(async (res: NativeGeocoderResult[]) => {
        let description = this.platform.is('ios') ? res[0].locality : res[0].subAdministrativeArea;
        const user = this.parse.currentUser;
        const customObj = {
        name: this.form.get('name').value,
        description: this.form.get('description').value,
        user: user,
        photo: await this.parse.saveFile('photo.jpg', this.addedPhoto),
        coords: new firebase.firestore.GeoPoint(lat, lng) as GeoPoint,
        location: description,
        class: this.customClass,
        type: this.customClass.get('type')
        };
    
        this.parse.saveItem('CustomObjects', customObj).then(async () => {
          this.utils.dismissLoading();
          this.modalCtrl.dismiss();
          await this.timer.stopTimer(5);
        }).catch(error => {
          this.utils.dismissLoading();
          this.utils.showToaster('Erro ao salvar objeto customizado');
          this.modalCtrl.dismiss();
        });
      });
    }
  }

}
