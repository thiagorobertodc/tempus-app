import {Component, NgZone, OnInit} from '@angular/core';
import {Geolocation} from '@ionic-native/geolocation/ngx';
import {Platform} from '@ionic/angular';
import {UtilsService} from '../../services/utils.service';
import {NativeGeocoder, NativeGeocoderOptions, NativeGeocoderResult} from '@ionic-native/native-geocoder/ngx';

declare var google: any;

@Component({
    selector: 'app-location-tag',
    templateUrl: './location-tag.component.html',
    styleUrls: ['./location-tag.component.scss'],
})
export class LocationTagComponent implements OnInit {
    public place: any;
    public notFound = true;
    public currentLat: number;
    public currentLng: number;

    constructor(public geolocation: Geolocation,
                public nativeGeocoder: NativeGeocoder,
                public zone: NgZone,
                public platform: Platform,
                public utils: UtilsService) {
    }

    async ngOnInit() {
        if (this.platform.is('cordova')) {
            console.log('test1');
            this.geolocation.getCurrentPosition({
                timeout: 10000,
                enableHighAccuracy: true,
                maximumAge: 3600
            }).then((resp) => {
                // console.log(JSON.stringify(resp));

                if (resp != null) {
                    this.currentLat = resp.coords.latitude;
                    this.currentLng = resp.coords.longitude;

                    console.log('currentLat= ' + JSON.stringify(this.currentLat));
                    console.log('currentLng= ' + JSON.stringify(this.currentLng));

                    const options: NativeGeocoderOptions = {
                        useLocale: true,
                        maxResults: 1
                    };
                    this.nativeGeocoder.reverseGeocode(this.currentLat, this.currentLng, options)
                        .then((res: NativeGeocoderResult[]) => {
                            // console.log(JSON.stringify(resp));

                            this.zone.run(() => {
                                this.place = this.platform.is('ios') ? res[0].locality : res[0].subAdministrativeArea;
                                this.notFound = false;
                            });
                        })
                        .catch((error: any) => {
                                console.log(JSON.stringify(error));
                            }
                        );
                }
            }).catch((error) => {
                console.log('Error getting location' + JSON.stringify(error.toString()));
                console.log('code: ' + error.code + '\n' +
                    'message: ' + error.message + '\n');
            });
        } else {
            this.geolocation.getCurrentPosition({
                timeout: 10000,
                enableHighAccuracy: true,
                maximumAge: 3600
            }).then((resp) => {
                this.currentLat = resp.coords.latitude;
                this.currentLng = resp.coords.longitude;

                const options: NativeGeocoderOptions = {
                    useLocale: true,
                    maxResults: 1
                };

                const geocoder = new google.maps.Geocoder();
                const latlng = {lat: resp.coords.latitude, lng: resp.coords.longitude};
                geocoder.geocode({location: latlng}, result => {
                    if (result[0]) {
                        this.zone.run(() => {
                            console.log(result[0]);
                            this.place = result[0].address_components[1].long_name + ' - ' + result[0].address_components[3].short_name;
                            this.notFound = false;
                        });
                    }
                });
            });
        }
    }
}
