import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NativeGeocoder } from '@ionic-native/native-geocoder/ngx';
import { IonicModule } from '@ionic/angular';
import {Geolocation} from '@ionic-native/geolocation/ngx';
import { LocationTagComponent } from './location-tag.component';
import {RouterTestingModule} from "@angular/router/testing";
declare var google: any;
class MockGeoLocationClass {
  getCurrentPosition: jasmine.Spy<any>;
}

class MockGeoCoderClass {
  reverseGeocode: jasmine.Spy<any>;
}

class MockResp {
   coords = {
   latitude: 20.11111111,
   longitude: 20.11111111
  };
}
describe('LocationTagComponent', () => {
  let component: LocationTagComponent;
  let fixture: ComponentFixture<LocationTagComponent>;
  let geolocationSpy, geocoderSpy, getCurrentPositionSpy, reverseSpy;
  beforeEach(async(() => {
    let resp = new MockResp(); 
    getCurrentPositionSpy = jasmine.createSpy().and.returnValue(Promise.resolve(resp));
    reverseSpy = jasmine.createSpy().and.returnValue(Promise.resolve());
    geolocationSpy = new MockGeoLocationClass();
    geolocationSpy.getCurrentPosition = getCurrentPositionSpy;
    geocoderSpy = new MockGeoCoderClass();
    geocoderSpy.reverseGeocode = reverseSpy;
    TestBed.configureTestingModule({
      declarations: [ LocationTagComponent ],
      imports: [IonicModule.forRoot(), RouterTestingModule],
      providers: [
        { provide: Geolocation, useValue: geolocationSpy },
        { provide: NativeGeocoder, useValue: geocoderSpy },
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(LocationTagComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

 
});
