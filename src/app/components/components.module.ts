import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {IonicModule} from '@ionic/angular';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {LocationTagComponent} from './location-tag/location-tag.component';
import {AngularSvgIconModule} from "angular-svg-icon";
import {EmptyComponent} from "./empty/empty.component";

@NgModule({
    declarations: [LocationTagComponent, EmptyComponent],
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule,
        AngularSvgIconModule,

    ],
    exports: [LocationTagComponent, EmptyComponent]
})
export class ComponentsModule {
}
