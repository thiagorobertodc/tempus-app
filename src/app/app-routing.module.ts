import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import {AuthGuard} from "./services/auth.guard";

const routes: Routes = [
  {path: '', redirectTo: 'tabs', pathMatch: 'full'},
  {
    path: 'tabs',
    loadChildren: () => import('./pages/tabs/tabs.module').then(m => m.TabsPageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'register',
    loadChildren: () => import('./pages/register/register.module').then( m => m.RegisterPageModule)
  },
  {
    path: 'forgot-password',
    loadChildren: () => import('./pages/forgot-password/forgot-password.module').then( m => m.ForgotPasswordPageModule)
  },
  {
    path: 'add-animal',
    loadChildren: () => import('./pages/modals/add-animal/add-animal.module').then( m => m.AddAnimalPageModule)
  },
  {
    path: 'add-relief',
    loadChildren: () => import('./pages/modals/add-relief/add-relief.module').then( m => m.AddReliefPageModule)
  },
  {
    path: 'add-building',
    loadChildren: () => import('./pages/modals/add-building/add-building.module').then( m => m.AddBuildingPageModule)
  },
  {
    path: 'add-tree',
    loadChildren: () => import('./pages/modals/add-tree/add-tree.module').then( m => m.AddTreePageModule)
  },
  {
    path: 'add-insect',
    loadChildren: () => import('./pages/modals/add-insect/add-insect.module').then( m => m.AddInsectPageModule)
  },
  {
    path: 'animal-detail/:id',
    loadChildren: () => import('./pages/animal-detail/animal-detail.module').then( m => m.AnimalDetailPageModule)
  },
  {
    path: 'building-detail/:id',
    loadChildren: () => import('./pages/building-detail/building-detail.module').then(m => m.BuildingDetailPageModule)
  },
  {
    path: 'insect-detail/:id',
    loadChildren: () => import('./pages/insect-detail/insect-detail.module').then( m => m.InsectDetailPageModule)
  },
  {
    path: 'relief-detail/:id',
    loadChildren: () => import('./pages/relief-detail/relief-detail.module').then( m => m.ReliefDetailPageModule)
  },
  {
    path: 'tree-detail/:id',
    loadChildren: () => import('./pages/tree-detail/tree-detail.module').then( m => m.TreeDetailPageModule)
  },
  {
    path: 'add-custom',
    loadChildren: () => import('./pages/modals/add-custom/add-custom.module').then( m => m.AddCustomPageModule)
  },
  {
    path: 'add-custom',
    loadChildren: () => import('./pages/modals/add-custom/add-custom.module').then( m => m.AddCustomPageModule)
  },
  {
    path: 'add-custom-object',
    loadChildren: () => import('./pages/modals/add-custom-object/add-custom-object.module').then( m => m.AddCustomObjectPageModule)
  },
  {
    path: 'custom-detail/:id',
    loadChildren: () => import('./pages/custom-detail/custom-detail.module').then( m => m.CustomDetailPageModule)
  },
  {
    path: 'about',
    loadChildren: () => import('./pages/about/about.module').then( m => m.AboutPageModule)
  },  {
    path: 'add-user',
    loadChildren: () => import('./pages/add-user/add-user.module').then( m => m.AddUserPageModule)
  }












];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
