import { Component } from '@angular/core';

import {ActionSheetController, MenuController, ModalController, Platform} from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import {AddAnimalPage} from "./pages/modals/add-animal/add-animal.page";
import {AddBuildingPage} from "./pages/modals/add-building/add-building.page";
import {AddReliefPage} from "./pages/modals/add-relief/add-relief.page";
import {AddInsectPage} from "./pages/modals/add-insect/add-insect.page";
import {AddTreePage} from "./pages/modals/add-tree/add-tree.page";
import {UtilsService} from "./services/utils.service";
import {Base64} from "@ionic-native/base64/ngx";
import {File} from '@ionic-native/file/ngx';
import {CameraService} from "./services/camera.service";
import { AddCustomPage } from './pages/modals/add-custom/add-custom.page';
import { ParseService } from './services/parse.service';
import { AddCustomObjectPage } from './pages/modals/add-custom-object/add-custom-object.page';
import { TimerService } from './services/timer.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  navigate : any;
  public customCategories: any = [];
  constructor(
    public platform: Platform,
    public splashScreen: SplashScreen,
    public statusBar: StatusBar,
    public modalCtrl: ModalController,
    public utils: UtilsService,
    public base64: Base64,
    public actionSheetCtrl: ActionSheetController,
    public file: File,
    public cameraService: CameraService,
    public menuCtrl: MenuController,
    public parse: ParseService,
    public timer: TimerService,
  ) {
    this.sideMenu();
    this.menuCtrl.enable(false);
    this.initializeApp();
  }

  ionViewWillEnter() {
      this.timer.startTimer();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.statusBar.backgroundColorByHexString('#41b3db');

            setTimeout(() => {
                this.splashScreen.hide();
            }, 1000);
      this.splashScreen.hide();

    });
  }

  fetchCategories() {
    this.parse.getList('CustomClass', [['user', '=', this.parse.currentUser]]).then(answer => {
          this.customCategories = answer;
          console.log(answer);
    });
  }

  sideMenu()
  {
    this.navigate =
        [
          {
            title : "Home",
            url   : "/home",
            icon  : "home"
          },
          {
            title : "Chat",
            url   : "/chat",
            icon  : "chatboxes"
          },
          {
            title : "Contacts",
            url   : "/contacts",
            icon  : "contacts"
          },
        ]
  }

  async addAnimal(data) {
    const modal = await this.modalCtrl.create({
        component: AddAnimalPage,
        keyboardClose: true,
        showBackdrop: true,
        componentProps: {
            photo: data
        },
        backdropDismiss: false
        
    });

    return await modal.present();
}

async changePicture(categoryNumber: any, id = null) {
    const actionsheet = await this.actionSheetCtrl.create({
        header: 'Escolha uma imagem',
        buttons: [
            {
                text: 'Tirar uma foto',
                icon: !this.platform.is('ios') ? 'camera' : null,
                handler: () => {
                    this.getPicture((data) => {
                        switch (categoryNumber) {
                            case 1:
                                this.addAnimal(data);
                                break;
                            case 2:
                                this.addRelief(data);
                                break;
                            case 3: 
                                this.addBug(data);
                                break;
                            case 4:
                                this.addBuilding(data);
                                break;
                            case 5:
                                this.addTree(data);
                                break;
                            case 6:
                                this.addCustomObject(data, id);
                            default:
                                break;
                        }
                    }, true);
                }
            },
            {
                text: 'Escolher da galeria',
                icon: !this.platform.is('ios') ? 'image' : null,
                handler: () => {
                    this.getPicture((data) => {
                        switch (categoryNumber) {
                            case 1:
                                this.addAnimal(data);
                                break;
                            case 2:
                                this.addRelief(data);
                                break;
                            case 3: 
                                this.addBug(data);
                                break;
                            case 4:
                                this.addBuilding(data);
                                break;
                            case 5:
                                this.addTree(data);
                                break;
                            case 6:
                                this.addCustomObject(data, id);
                            default:
                                break;
                        }
                    }, false);
                }
            },
            {
                text: 'Cancelar',
                icon: !this.platform.is('ios') ? 'close' : null,
                role: 'destructive',
                handler: () => {
                    // console.log('the user has cancelled the interaction.');
                }
            }
        ]
    });
    await actionsheet.present();
}

async takeAnimalPicture() {
    this.timer.startTimer();
    await this.changePicture(1);
}

// openModalWithoutData(caseNumber, id = null) {

//     switch(caseNumber){
//         case 1:
//             this.addAnimal();
//             break;
//         case 2:
//             this.addRelief();
//             break;
//         case 3:
//             this.addBug();
//             break;
//         case 4:
//             this.addBuilding();
//             break;
//         case 5:
//             this.addTree();
//             break;
//         case 6:
//             this.addCustomObject('', id);

//     }
// }

async takeBuildingPicture() {
    this.timer.startTimer();
    await this.changePicture(4);
}

async takeReliefPicture() {
    this.timer.startTimer();
    await this.changePicture(2);
}

async takeBugPicture() {
    this.timer.startTimer();
    await this.changePicture(3);
}

async takeTreePicture() {
    this.timer.startTimer();
    await this.changePicture(5);
}

async takeCustomImage(id) {
    this.timer.startTimer();
    await this.changePicture(6, id);
}
 
  async addBuilding(data) {
        const modal = await this.modalCtrl.create({
            component: AddBuildingPage,
            keyboardClose: true,
            showBackdrop: true,
            componentProps: {
                photo: data,
            },
            backdropDismiss: false
        });

        return await modal.present();
    }

    async addRelief(data) {
        const modal = await this.modalCtrl.create({
            component: AddReliefPage,
            keyboardClose: true,
            showBackdrop: true,
            componentProps: {
                photo: data,
            },
            backdropDismiss: false
        });

        return await modal.present();
    }

    async addBug(data) {
        const modal = await this.modalCtrl.create({
            component: AddInsectPage,
            keyboardClose: true,
            showBackdrop: true,
            componentProps: {
                photo: data,
            },
            backdropDismiss: false
        });

        return await modal.present();
    }

    async addTree(data) {
        const modal = await this.modalCtrl.create({
            component: AddTreePage,
            keyboardClose: true,
            showBackdrop: true,
            componentProps: {
                photo: data,
            },
            backdropDismiss: false
        });

        return await modal.present();
    }

    async addCustom(data) {       
        this.timer.startTimer();
        const modal = await this.modalCtrl.create({
            component: AddCustomPage,
            keyboardClose: true,
            showBackdrop: true,
            backdropDismiss: false,
        });

        return await modal.present();
    }

    async addCustomObject(data, id) {
        this.timer.startTimer();
        const modal = await this.modalCtrl.create({
            component: AddCustomObjectPage,
            keyboardClose: true,
            showBackdrop: true,
            backdropDismiss: false,
            componentProps: {
                id: id,
                photo: data,
            }
        });
        return await modal.present();
    }


    getPicture(callback, camera = true, height = null, width = null) {

        if (this.platform.is('cordova')) {
            return this.cameraService.getMediaDefault(camera, height, width).then(result => {
                if (!result.error && result.data) {
                    if (this.utils.platform.is('android')) {
                        this.base64.encodeFile(result.data).then((base64File: string) => {
                            base64File = (base64File.split('base64,'))[1];
                            // console.log('base= ' + base64File);
                            // console.log(base64File);
                            callback(base64File);
                        }, (err) => {
                            // this.actions.dismissLoading();
                            // console.log(err);
                        });
                    } else {
                        // split file path to directory and file name
                        const fileName = result.data.split('/').pop();
                        const path = result.data.substring(0, result.data.lastIndexOf('/') + 1);

                        this.file.readAsDataURL(path, fileName)
                            .then(base64File => {
                                // console.log("here is encoded image ", base64File);
                                base64File = (base64File.split('base64,'))[1];
                                // console.log(base64File);
                                callback(base64File);
                            })
                            .catch((err) => {
                                // console.log('Error reading file= ' + JSON.stringify(err));
                            });
                    }
                }
            }).catch(error => {
                this.utils.dismissLoading();
                // console.log(error.data);
                // this.utils.showToaster('Ocorreu algum erro: ' + error.data);
            });
        } else {
            this.utils.showToaster('Somente disponível no mobile', 2000);
        }
    }
}
