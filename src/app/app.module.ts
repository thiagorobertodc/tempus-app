import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import {HttpClientModule} from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {Camera} from '@ionic-native/camera/ngx';
import {Crop} from '@ionic-native/crop/ngx';
import {Base64} from '@ionic-native/base64/ngx';
import {File} from '@ionic-native/file/ngx';
import {Base64ToGallery} from '@ionic-native/base64-to-gallery/ngx';
import {AddAnimalPageModule} from "./pages/modals/add-animal/add-animal.module";
import {AddBuildingPageModule} from "./pages/modals/add-building/add-building.module";
import {AddReliefPageModule} from "./pages/modals/add-relief/add-relief.module";
import {AddInsectPageModule} from "./pages/modals/add-insect/add-insect.module";
import {AddTreePageModule} from "./pages/modals/add-tree/add-tree.module";
import {Geolocation} from '@ionic-native/geolocation/ngx';
import {AngularSvgIconModule} from 'angular-svg-icon';
import {NativeGeocoder} from "@ionic-native/native-geocoder/ngx";
import { AvatarModule } from 'ngx-avatar';
import { AddCustomPageModule } from './pages/modals/add-custom/add-custom.module';
import { AddCustomObjectPageModule } from './pages/modals/add-custom-object/add-custom-object.module';
// @ts-ignore
@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    AddAnimalPageModule,
    AddBuildingPageModule,
    AddReliefPageModule,
    AddInsectPageModule,
    AddTreePageModule,
    AddCustomPageModule,
    AvatarModule,
    HttpClientModule,
    AddCustomObjectPageModule,
    AngularSvgIconModule.forRoot()],
  providers: [
    StatusBar,
    SplashScreen,
    Geolocation,
    NativeGeocoder,
    Base64,
    Base64ToGallery,
    File,
    Camera,
    Crop,
    AvatarModule,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})

export class AppModule {}
