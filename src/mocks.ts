import * as jasmine from 'jasmine-core';

export class ParseMock {
public logIn: Function = (email: string, password: string) => {};
public getList: Function = (table: string, params: []) => {};
public saveItem: Function = (table: string, params: any) => Object;
public addAnimal: Function = () => {};
}

export class MockTimer {
    startTimer: jasmine.Spy<any>;
    stopTimer: jasmine.Spy<any>;
  }
export  class MockParse {
    getList: jasmine.Spy<any>;
    getItem: jasmine.Spy<any>;
    saveItem: jasmine.Spy<any>;
  }